package com.store.mobin.treehousestd.store.app.activities;

import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.RatingBar;
import android.widget.Toast;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.store.mobin.treehousestd.store.R;
import com.store.mobin.treehousestd.store.app.models.User;
import com.store.mobin.treehousestd.store.app.utils.utils;

/**
 * Created by Treehouse.std on 3/30/2017.
 */

public class FeedBack extends BaseActivity {
    EditText email;
    RatingBar ratingBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.thank_you);
        hideKeyboard();
        initializeViews();
    }


    @Override
    protected void initializeViews() {
        email = (EditText) findViewById(R.id.email_feedback);
        ratingBar = (RatingBar) findViewById(R.id.rate_us);
        email.setText(User.getUserEmail());
    }

    public void onFinish(View view) {
        String emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";
        if (email.getText().toString().matches(emailPattern)) {
            rateApp(ratingBar.getRating());
        } else {
            Toast.makeText(this, "invalid email", Toast.LENGTH_SHORT).show();

        }
    }

    void rateApp(Float rating){
        DatabaseReference databaseReference = FirebaseDatabase.getInstance().getReference(getString(R.string.app_name)).child("users");
        databaseReference.child(User.getUserID()).child("rating").setValue(rating);
        Toast.makeText(this,"Thank You", Toast.LENGTH_SHORT).show();
    }
    @Override
    protected void onResume() {
        super.onResume();
        utils.secondTime = true;
    }

    @Override
    public void onBackPressed() {
        utils.secondTime = true;
        finish();
    }

}
