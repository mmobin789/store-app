package com.store.mobin.treehousestd.store.app.models;

import android.support.annotation.NonNull;

import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import java.util.ArrayList;

/**
 * Created by Treehouse.std on 3/25/2017.
 */

public class Friend {
    public static ArrayList<Friend> friendsList = new ArrayList<>();
    public static ArrayList<Item> items2Friend = new ArrayList<>();
    public static List list2Friend;
    public static String id = "", name = "", email = "";
    private String senderID, receiverID;
    private String requestID;
    private String friendID;
    private String senderEmail, receiverEmail;
    private int requestStatus;
    private String senderName, receiverName;

    public static String getFriendRequestCount() {
        int count = 0;

        for (int i = 0; i < friendsList.size(); i++) {
            if (friendsList.get(i).getRequestStatus() == 0 && friendsList.get(i).getReceiverID().equals(User.getUserID())) {
                count++;
            }
        }
        if (count == 0)
            return "";
        else
            return count + "";
    }

    @NonNull
    public static StorageReference getFriendAvatarRef(String id) {
        FirebaseStorage firebaseStorage = FirebaseStorage.getInstance();
        return firebaseStorage.getReference("Your Shopping App").child("users").child("images/" + id);
    }

    public String getFriendID() {
        return friendID;
    }

    public void setFriendID(String friendID) {
        this.friendID = friendID;
    }

    public String getRequestID() {
        return requestID;
    }

    public void setRequestID(String requestID) {
        this.requestID = requestID;
    }

    public String getSenderName() {
        return senderName;
    }

    public void setSenderName(String senderName) {
        this.senderName = senderName;
    }

    public String getReceiverName() {
        return receiverName;
    }

    public void setReceiverName(String receiverName) {
        this.receiverName = receiverName;
    }

    public int getRequestStatus() {
        return requestStatus;
    }

    public void setRequestStatus(int requestStatus) {
        this.requestStatus = requestStatus;
    }


    public String getSenderID() {
        return senderID;
    }

    public void setSenderID(String senderID) {
        this.senderID = senderID;
    }

    public String getReceiverID() {
        return receiverID;
    }

    public void setReceiverID(String receiverID) {
        this.receiverID = receiverID;
    }

    public String getSenderEmail() {
        return senderEmail;
    }

    public void setSenderEmail(String senderEmail) {
        this.senderEmail = senderEmail;
    }

    public String getReceiverEmail() {
        return receiverEmail;
    }

    public void setReceiverEmail(String receiverEmail) {
        this.receiverEmail = receiverEmail;
    }
}
