package com.store.mobin.treehousestd.store.app.activities;

import android.app.ProgressDialog;
import android.content.Context;
import android.net.ConnectivityManager;
import android.support.v7.app.AppCompatActivity;
import android.view.WindowManager;

/**
 * Created by Treehouse.std on 3/19/2017.
 */

public class BaseActivity extends AppCompatActivity {
    private ProgressDialog progressDialog;

    protected void hideKeyboard() {
        getWindow().setSoftInputMode(
                WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN
        );
    }

    protected void closeProgressDialog() {
        progressDialog.dismiss();
    }

    protected void showProgressDialog(Context context, String msg) {
        progressDialog = new ProgressDialog(context);
        progressDialog.setMessage(msg);
        progressDialog.setIndeterminate(true);
        progressDialog.setCancelable(false);
        progressDialog.show();
    }

    protected boolean isNetworkConnected() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(CONNECTIVITY_SERVICE);

        return cm.getActiveNetworkInfo() != null;
    }

    protected void setClickListeners() {

    }

    protected void initializeViews() {


    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

}
