package com.store.mobin.treehousestd.store.app.activities;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.EditText;
import android.widget.PopupMenu;
import android.widget.TextView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.store.mobin.treehousestd.store.R;
import com.store.mobin.treehousestd.store.app.adapters.MyAdapter;
import com.store.mobin.treehousestd.store.app.models.Friend;
import com.store.mobin.treehousestd.store.app.models.List;
import com.store.mobin.treehousestd.store.app.models.User;
import com.store.mobin.treehousestd.store.app.utils.utils;

import java.util.LinkedList;

import static com.store.mobin.treehousestd.store.app.models.Friend.friendsList;

public class ListsActivity extends BaseActivity {

    Toolbar toolbar;
    TextView plus, signOut, addContact, friendRequestCount, friendRequest;
    RecyclerView recyclerView;
    MyAdapter recyclerAdapter;
    LinkedList<List> projectsList;
    DatabaseReference databaseReference;
    ValueEventListener valueEventListener = new ValueEventListener() {
        @Override
        public void onDataChange(DataSnapshot dataSnapshot) {
            projectsList.clear();
            for (DataSnapshot dataSnapshots : dataSnapshot.getChildren()) {
                List savedProjects = dataSnapshots.getValue(List.class);
                //  savedProjects.setListID(dataSnapshots.getKey());
                projectsList.add(savedProjects);
            }

            recyclerAdapter.notifyDataSetChanged();
            closeProgressDialog();


        }

        @Override
        public void onCancelled(DatabaseError databaseError) {

        }
    };
    private Query userFriends;
    ValueEventListener getUserFriendsEvent = new ValueEventListener() {
        @Override
        public void onDataChange(DataSnapshot dataSnapshot) {
            friendsList.clear();

            for (DataSnapshot ds : dataSnapshot.getChildren()) {
                Friend friend = ds.getValue(Friend.class);
                if(User.getUserEmail().equals(friend.getReceiverEmail())) {
                    friendsList.add(friend);
                }
                if(User.getUserEmail().equals(friend.getSenderEmail()))
                {
                    friendsList.add(friend);
                }


            }
            friendRequestCount.setText(Friend.getFriendRequestCount());
            if (userFriends != null)
                userFriends.removeEventListener(getUserFriendsEvent);


        }

        @Override
        public void onCancelled(DatabaseError databaseError) {

        }
    };

    private void getFriends() {

        userFriends = FirebaseDatabase.getInstance().getReference(getString(R.string.app_name)).child("Friends");
        userFriends.orderByChild("senderEmail").equals(User.getUserEmail());
        userFriends.addListenerForSingleValueEvent(getUserFriendsEvent);


    }

    private void removeEventListener() {
        if (databaseReference != null)
            databaseReference.removeEventListener(valueEventListener);
        if (userFriends != null)
            userFriends.removeEventListener(getUserFriendsEvent);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //   utils.getFacebookKeyHash(this);
        if (isNetworkConnected()) {
            setContentView(R.layout.list_activity);
            initializeViews();
            setClickListeners();
            getProjects();

        } else {
            utils.noNetDialog(this);
        }
    }

    void addProjectDialog() {
        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.add_project_dialog);
        final EditText project = (EditText) dialog.findViewById(R.id.input);
        final EditText des = (EditText) dialog.findViewById(R.id.list_Des);
        project.setText("List " + (projectsList.size() + 1) + "");

        dialog.findViewById(R.id.add).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!utils.validInput(project)) {
                    project.requestFocus();
                } else {
                    addProject(project.getText().toString().trim(), des.getText().toString().trim());
                    dialog.dismiss();
                }

                //  Toast.makeText(v.getContext(), getAddedProjectID(), Toast.LENGTH_SHORT).show();


            }
        });
        dialog.findViewById(R.id.cancel).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();

                //     Toast.makeText(v.getContext(),user.getUsername(),Toast.LENGTH_SHORT).show();
            }
        });
        dialog.show();


    }

    void addProject(String projectName, String description) {
        List list = new List();
        list.setName(projectName);
        list.setDescription(description);
        list.setId("lid" + utils.removeSpaces(projectName) + utils.getRand());
        projectsList.add(list); // add list to list list
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        databaseReference = database.getReference(getString(R.string.app_name)).child("users").child(User.getUserID()).child("Lists");
        databaseReference.child(list.getId()).setValue(list);
        recyclerAdapter.notifyDataSetChanged();


    }

    @Override
    protected void onStart() {
        super.onStart();
        getFriends();
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (utils.secondTime) {
            utils.secondTime = false;

            getProjects();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        removeEventListener();

    }

    @Override
    protected void onStop() {
        super.onStop();
        removeEventListener();
    }


  public void getProjects() {

        FirebaseDatabase database = FirebaseDatabase.getInstance();
        databaseReference = database.getReference(getString(R.string.app_name)).child("users").child(User.getUserID()).child("Lists");
        showProgressDialog(this, "Loading Lists...");
        databaseReference.addValueEventListener(valueEventListener);
    }

    @Override
    protected void setClickListeners() {
        plus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addProjectDialog();
            }
        });
        friendRequest.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(v.getContext(), Friends.class));
            }
        });
        signOut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showPopup(v);
            }
        });
    }

    private void showPopup(View v) {
        PopupMenu popup = new PopupMenu(v.getContext(), v);
        MenuInflater inflater = popup.getMenuInflater();
        inflater.inflate(R.menu.signout, popup.getMenu());
        popup.show();
        popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.signOut:
                        FirebaseAuth.getInstance().signOut();
                        startActivity(new Intent(ListsActivity.this, LoginActivity.class));
                        finish();    // signed out close app
                        break;
                    case R.id.editProfile:
                        startActivity(new Intent(ListsActivity.this, EditProfile.class));
                        break;
                    case R.id.security:
                        startActivity(new Intent(ListsActivity.this, UpdatePassword.class));
                        break;
                    case R.id.rate_us:
                        startActivity(new Intent(ListsActivity.this,FeedBack.class));
                        break;

                }

                return true;
            }
        });
    }

    @Override
    protected void initializeViews() {
        projectsList = new LinkedList<>();
        toolbar = (Toolbar) findViewById(R.id.toolbar_top);
        addContact = (TextView) findViewById(R.id.addContact);
        plus = (TextView) toolbar.findViewById(R.id.toolbar_plus);
        friendRequestCount = (TextView) findViewById(R.id.friendRequestsCount);
        friendRequest = (TextView) findViewById(R.id.friendRequests);
        signOut = (TextView) toolbar.findViewById(R.id.signOut);
        recyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        utils.setFontAwesomeOnTextViews(this, new TextView[]{plus, signOut, addContact, friendRequest});
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);
        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(recyclerView.getContext(),
                layoutManager.getOrientation());
        recyclerView.addItemDecoration(dividerItemDecoration);
        recyclerAdapter = new MyAdapter(projectsList, ListsActivity.this);
        recyclerAdapter.setHasStableIds(true);
        recyclerView.setAdapter(recyclerAdapter);
        //   getAddedProjects();


    }
}
