package com.store.mobin.treehousestd.store.app.activities;

import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.firebase.ui.storage.images.FirebaseImageLoader;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.UserProfileChangeRequest;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.mvc.imagepicker.ImagePicker;
import com.store.mobin.treehousestd.store.R;
import com.store.mobin.treehousestd.store.app.models.User;
import com.store.mobin.treehousestd.store.app.utils.utils;

import java.io.ByteArrayOutputStream;

import de.hdodenhof.circleimageview.CircleImageView;

public class EditProfile extends BaseActivity {
    TextView back;
    CircleImageView profilePic;
    DatabaseReference currentUser;
    EditText userName, email, phn;
    TextView submit;
    Bitmap userPic;
    ValueEventListener eventListener = new ValueEventListener() {
        @Override
        public void onDataChange(DataSnapshot dataSnapshot) {

            User user = dataSnapshot.getValue(User.class);
            if (user != null)
                phn.setText(user.getPhn());


        }

        @Override
        public void onCancelled(DatabaseError databaseError) {

        }
    };

    void removeEventListener() {
        if (currentUser != null)
            currentUser.removeEventListener(eventListener);
    }

    @Override
    protected void onPause() {
        super.onPause();
        removeEventListener();
    }

    @Override
    protected void onStop() {
        super.onStop();
        removeEventListener();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_profile);
        initializeViews();
        showCurrentProfile();
        setClickListeners();

    }

    @Override
    protected void initializeViews() {
        profilePic = (CircleImageView) findViewById(R.id.profile_image);
        userName = (EditText) findViewById(R.id.userName);
        email = (EditText) findViewById(R.id.email);
        phn = (EditText) findViewById(R.id.phn);
        submit = (TextView) findViewById(R.id.submit);
        back = (TextView) findViewById(R.id.back);
        utils.setFontAwesomeOnTextViews(this, new TextView[]{back});
    }

    @Override
    protected void setClickListeners() {
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                updateProfile();
            }
        });
    }

    public void addAvatar(View v) {
        ImagePicker.pickImage(this);
        ImagePicker.setMinQuality(512, 512);
    }

    public void showCurrentProfile() {
        //Glide.with(this).using(new FirebaseImageLoader()).load(User.getUserPic()).into(profilePic);
        Glide.with(this).using(new FirebaseImageLoader()).load(User.getUserPic()).diskCacheStrategy(DiskCacheStrategy.NONE).skipMemoryCache(true).into(profilePic);
        email.setText(User.getUserEmail());
        userName.setText(User.getUserName());
        currentUser = FirebaseDatabase.getInstance().getReference(getString(R.string.app_name)).child("users").child(User.getUserID());

        currentUser.addValueEventListener(eventListener);

    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        userPic = ImagePicker.getImageFromResult(this, requestCode, resultCode, data);
        if (userPic != null) {
            profilePic.setImageBitmap(userPic);
        }
    }


    void saveONFirebaseAndSetProfile(User user) {

        DatabaseReference databaseReference = FirebaseDatabase.getInstance().getReference(getString(R.string.app_name)).child("users");
        databaseReference.child(User.getUserID()).child("username").setValue(user.getUsername());
        databaseReference.child(User.getUserID()).child("phn").setValue(user.getPhn());
        UserProfileChangeRequest userProfileChangeRequest = new UserProfileChangeRequest.Builder().setDisplayName(user.getUsername()).build();
        if (userPic != null) {
            saveAvatar2Firebase(User.getUserID());
        }
        FirebaseAuth firebaseAuth = FirebaseAuth.getInstance();
        if (firebaseAuth.getCurrentUser() != null) {
            firebaseAuth.getCurrentUser().updateProfile(userProfileChangeRequest).addOnCompleteListener(this, new OnCompleteListener<Void>() {
                @Override
                public void onComplete(@NonNull Task<Void> task) {
                    if (task.isSuccessful()) {
                        Toast.makeText(EditProfile.this, "Update Successful", Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(EditProfile.this, task.getException().getMessage(), Toast.LENGTH_LONG).show();
                        Log.d("Error", task.getException().toString());
                    }
                }
            });
        }
    }

    void saveAvatar2Firebase(final String uID) {
        FirebaseStorage firebaseStorage = FirebaseStorage.getInstance();
        StorageReference storageReference = firebaseStorage.getReference(getString(R.string.app_name)).child("users").child("images/" + User.getUserID());
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        userPic.compress(Bitmap.CompressFormat.PNG, 100, baos);
        byte[] data = baos.toByteArray();
        UploadTask uploadUserPic = storageReference.putBytes(data);
        uploadUserPic.addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
            @Override
            public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                Log.i("uploadSuccess", taskSnapshot.getDownloadUrl() + "");
            }

        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                Log.e("uploadFailedtryAgain", e.getMessage());
                saveAvatar2Firebase(uID);
            }
        });
    }

    void updateUserData() {
        User user = new User();
        if (utils.isEmailValid(email)) {
            user.setEmail(email.getText().toString());
            user.setUsername(userName.getText().toString());
            user.setPhn(phn.getText().toString());
            saveONFirebaseAndSetProfile(user);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        utils.secondTime = true;
    }

    @Override
    public void onBackPressed() {
        utils.secondTime = true;
        finish();
    }

    void updateProfile() {
        updateUserData();
    }
}
