package com.store.mobin.treehousestd.store.app.activities;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.NotificationManager;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.store.mobin.treehousestd.store.R;
import com.store.mobin.treehousestd.store.app.models.Reminder;
import com.store.mobin.treehousestd.store.app.models.User;
import com.store.mobin.treehousestd.store.app.receivers.NotificationReceiver;
import com.store.mobin.treehousestd.store.app.utils.AlarmUtils;
import com.store.mobin.treehousestd.store.app.utils.utils;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

/**
 * Created by Treehouse.std on 3/16/2017.
 */

public class SettingsActivity extends BaseActivity {
    TextView back,setReminder,deleteReminder;
    RelativeLayout notification, reminder;
    CheckBox remCheck, notifCheck;
    String listName,listId;
    Reminder user;
    Reminder reminderDB;
    DatabaseReference d;
    Spinner spinner;
    String reminderText;
    Boolean isDateSet = false;
    Boolean isTimeSet = false;
    ArrayAdapter<String> dataAdapter;
    ValueEventListener eventListener = new ValueEventListener() {
        @Override
        public void onDataChange(DataSnapshot dataSnapshot) {
            reminderDB = dataSnapshot.getValue(Reminder.class);

            if (reminderDB != null) {
                remCheck.setChecked(reminderDB.getReminderCheck());
                notification.setClickable(reminderDB.getReminderCheck());
                notifCheck.setChecked(reminderDB.getReminderCheck());
                int spinnerPosition = dataAdapter.getPosition(reminderDB.getBeforeReminder());
                spinner.setSelection(spinnerPosition);
            }


        }

        @Override
        public void onCancelled(DatabaseError databaseError) {

        }
    };
    private int reminderMinutes;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.settings);
        initializeViews();
        setClickListeners();
        getReminder();
        setSpinner();
    }

    @Override
    protected void initializeViews() {

        TextView n = (TextView) findViewById(R.id.notif);
        TextView r = (TextView) findViewById(R.id.reminder);
        spinner = (Spinner) findViewById(R.id.spinner);
        back = (TextView) findViewById(R.id.back);
        reminder = (RelativeLayout) findViewById(R.id.reminderNav);
        notification = (RelativeLayout) findViewById(R.id.notifNav);
        remCheck = (CheckBox) findViewById(R.id.remCheck);
        notifCheck = (CheckBox) findViewById(R.id.notifCheck);
        utils.setFontAwesomeOnTextViews(this, new TextView[]{n, r, back});
        Intent get = getIntent();
        listName = get.getStringExtra("list name");
        listId = get.getStringExtra("list id");
        reminderMinutes = 0;
        setReminder = (TextView) findViewById(R.id.set_reminder);
        deleteReminder = (TextView) findViewById(R.id.delete_reminder);
        reminderText = "";
        user = new Reminder();
    }

    void getReminder() {
        FirebaseAuth auth = FirebaseAuth.getInstance();
        if (auth.getCurrentUser() != null) {
            d = FirebaseDatabase.getInstance().getReference(getString(R.string.app_name)).child("users").child(User.getUserID()).child("Lists").child(listId).child("Reminder");

            d.addValueEventListener(eventListener);
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (d != null)
            d.removeEventListener(eventListener);
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (d != null)
            d.removeEventListener(eventListener);
    }

    void openDatePickerDialog(View view, final TextView dateView) {

        DatePickerDialog.OnDateSetListener dateSetListener = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                Calendar calendar = Calendar.getInstance();
                calendar.set(Calendar.YEAR, year);
                calendar.set(Calendar.MONTH, month);
                calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);

                SimpleDateFormat sdf = new SimpleDateFormat("EEE, dd/MMMM/yyyy", Locale.ENGLISH);

                dateView.setText(sdf.format(calendar.getTime()));
                user.setYear(year);
                user.setMonth(month + 1);
                user.setDay(dayOfMonth);
                isDateSet = true;

            }
        };
        Calendar calendar = Calendar.getInstance();
        DatePickerDialog datePickerDialog = new DatePickerDialog(view.getContext(), R.style.AppTheme, dateSetListener, calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH));
        datePickerDialog.setTitle("Select Date");
        datePickerDialog.setCancelable(false);
        datePickerDialog.getDatePicker().setMinDate(calendar.getTimeInMillis());
        datePickerDialog.show();


    }

    void openTimePickerDialog(View v, final TextView timeView) {
        // TODO Auto-generated method stub
        Calendar mcurrentTime = Calendar.getInstance();
        int hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
        int minute = mcurrentTime.get(Calendar.MINUTE);
        TimePickerDialog mTimePicker;

        mTimePicker = new TimePickerDialog(this, new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {
                String amPm;
                Calendar present = Calendar.getInstance();
                if (selectedHour < 12)
                    amPm = "AM";
                else
                    amPm = "PM";
                if (selectedHour >= present.get(Calendar.HOUR_OF_DAY)) {
                    isTimeSet = true;
                    if(selectedHour == present.get(Calendar.HOUR_OF_DAY))
                    {
                        isTimeSet = selectedMinute > present.get(Calendar.MINUTE);
                    }
                } else {
                    isTimeSet = false;
                }


                //         SimpleDateFormat sdf = new SimpleDateFormat("hh:mm aa",Locale.ENGLISH);

                timeView.setText(selectedHour + ":" + selectedMinute + " " + amPm);

                user.setHour(selectedHour);
                user.setMinute(selectedMinute);
                user.setAmOrPm(amPm);
                user.setAlarmCode(utils.getRand());
            }
        }, hour, minute, false);//Yes 24 hour time
        mTimePicker.setTitle("Select Time");
        mTimePicker.show();

    }

    void loadDialog(View v) {
        final Dialog dialog = new Dialog(v.getContext());
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.notification);
        final EditText reminder = (EditText) dialog.findViewById(R.id.input);

        //   EditText title = (EditText) dialog.findViewById(R.id.inputTitle);
        Button save = (Button) dialog.findViewById(R.id.add);
        final TextView dateView = (TextView) dialog.findViewById(R.id.dateView);
        final TextView timeView = (TextView) dialog.findViewById(R.id.timeView);
        if (reminderDB != null) {
            reminder.setText(reminderDB.getReminder());
            dateView.setText(reminderDB.getDay() + "/" + reminderDB.getMonth() + "/" + reminderDB.getYear());
            timeView.setText(reminderDB.getHour() + ":" + reminderDB.getMinute() + " " + reminderDB.getAmOrPm());
        } else {
            Calendar cal = Calendar.getInstance();
            reminder.setText("");
            dateView.setText("" + cal.get(Calendar.DATE) + "/" + (cal.get(Calendar.MONTH) + 1) + "/" + cal.get(Calendar.YEAR));
            SimpleDateFormat sdf = new SimpleDateFormat("hh:mm aa", Locale.ENGLISH);
            timeView.setText(sdf.format(cal.getTime()));
        }
        timeView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openTimePickerDialog(v, timeView);
            }
        });
        dateView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openDatePickerDialog(v, dateView);
            }
        });
        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (!utils.validInput(reminder)) {
                    //saveReminderToFireBase(reminder.getText().toString(), false);

                    Toast.makeText(v.getContext(), reminder.getHint(), Toast.LENGTH_SHORT).show();

                } else if (isDateSet && isTimeSet) {
                    reminderText = reminder.getText().toString().trim();
                    dialog.dismiss();
                } else {
                    Toast.makeText(v.getContext(), "Notification can't be set for present/past time", Toast.LENGTH_LONG).show();
                }
            }
        });
        dialog.findViewById(R.id.cancel).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        dialog.show();
    }

    private void saveReminderToFireBase(String reminder, boolean check) {
        FirebaseAuth auth = FirebaseAuth.getInstance();
        if (!check) {
            reminder = "";
        }
        if (auth.getCurrentUser() != null) {
            DatabaseReference d = FirebaseDatabase.getInstance().getReference(getString(R.string.app_name)).child("users").child(User.getUserID()).child("Lists").child(listId).child("Reminder");
            user.setReminder(reminder);
            user.setReminderCheck(check);
            d.setValue(user);
            remCheck.setChecked(check);
            notification.setClickable(check);
            notifCheck.setChecked(check);
            createNotification(reminder);
        }

    }

    void createNotification(String reminder) {

        Calendar c = Calendar.getInstance();
        c.set(Calendar.MONTH, user.getMonth() - 1);
        c.set(Calendar.YEAR, user.getYear());
        c.set(Calendar.DAY_OF_MONTH, user.getDay());
        c.set(Calendar.HOUR_OF_DAY, user.getHour());
        c.set(Calendar.MINUTE, user.getMinute());
        c.set(Calendar.SECOND, 0);

        if (user.getHour() < 12) {
            c.set(Calendar.AM_PM, Calendar.AM);
        } else {
            c.set(Calendar.AM_PM, Calendar.PM);
        }
        long beforeReminderTime = reminderMinutes*60*1000;
        long reminderTime = c.getTimeInMillis() - beforeReminderTime;
        c.setTimeInMillis(reminderTime);
        Intent myIntent = new Intent(this, NotificationReceiver.class);
        myIntent.putExtra("reminder", reminder);
        myIntent.putExtra("code",user.getAlarmCode());
        AlarmUtils.addAlarm(getApplicationContext(),myIntent,user.getAlarmCode(),c);
        Toast.makeText(this, "Notification scheduled for " + c.getTime(), Toast.LENGTH_LONG).show();


    }

    @Override
    public void onBackPressed() {
        utils.secondTime = true;
        finish();
    }

    @Override
    protected void setClickListeners() {
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                utils.secondTime = true;
                onBackPressed();
            }
        });
        reminder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                loadDialog(v);

            }
        });
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long id) {
                String item = adapterView.getItemAtPosition(position).toString();
                user.setBeforeReminder(item);
                reminderMinutes = getReminderTime(item);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
        setReminder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(isDateSet && isTimeSet){
                    saveReminderToFireBase(reminderText, true);
                }else{
                    Toast.makeText(SettingsActivity.this,"Set Date and Time First",Toast.LENGTH_SHORT).show();
                }
            }
        });
        deleteReminder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                deleteAlarm();
            }
        });
    }
    void setSpinner(){
        List<String> categories = new ArrayList<String>();
        categories.add("Set Reminder");
        categories.add("On time");
        categories.add("1 min before");
        categories.add("5 min before");
        categories.add("10 min before");
        categories.add("15 min before");
        categories.add("20 min before");
        categories.add("30 min before");
        categories.add("45 min before");
        categories.add("1 hour before");
        categories.add("2 hour before");
        categories.add("3 hour before");
        categories.add("12 hour before");
        dataAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, categories);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(dataAdapter);
    }


    int getReminderTime(String time){
        int minutes = 0;
        switch (time){

            case "Set Reminder":
                minutes = 0;
                break;
            case "On time":
                minutes = 0;
                break;
            case "1 min before":
                minutes = 1;
                break;
            case "5 min before":
                minutes = 5;
                break;
            case "10 min before":
                minutes = 10;
                break;
            case "15 min before":
                minutes = 15;
                break;
            case "20 min before":
                minutes = 20;
                break;
            case "30 min before":
                minutes = 30;
                break;
            case "45 min before":
                minutes = 45;
                break;
            case "1 hour before":
                minutes = 60;
                break;
            case "2 hour before":
                minutes = 120;
                break;
            case "3 hour before":
                minutes = 180;
                break;
            case "12 hour before":
                minutes = 720;
                break;
        }
        return minutes;
    }
    void deleteAlarm(){
        if(reminderDB!=null){
            NotificationManager notificationManager = (NotificationManager)getSystemService(NOTIFICATION_SERVICE);
            notificationManager.cancel(reminderDB.getAlarmCode());
            Intent myIntent = new Intent(this,NotificationReceiver.class);
            AlarmUtils.cancelAlarm(getApplicationContext(),myIntent,reminderDB.getAlarmCode());
            deleteReminderFromFirebase();
            refreshActivity();
            Toast.makeText(SettingsActivity.this,"Alarm Deleted",Toast.LENGTH_SHORT).show();
        }else{
            Toast.makeText(SettingsActivity.this,"Set Alarm First",Toast.LENGTH_SHORT).show();
        }
    }
    private void deleteReminderFromFirebase() {
        DatabaseReference databaseReference = FirebaseDatabase.getInstance().getReference(getString(R.string.app_name)).child("users").child(User.getUserID()).child("Lists").child(listId).child("Reminder");
        databaseReference.removeValue();
    }
    private void refreshActivity(){
        Intent intent = getIntent();
        overridePendingTransition(0, 0);
        intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
        finish();
        overridePendingTransition(0, 0);
        startActivity(intent);
    }
}
