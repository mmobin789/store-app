package com.store.mobin.treehousestd.store.app.activities;

import android.annotation.SuppressLint;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.text.format.DateFormat;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;

import com.store.mobin.treehousestd.store.R;
import com.store.mobin.treehousestd.store.app.utils.utils;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class PlanShoppingDate extends BaseActivity {

     TextView date,time;
    TextView setPlan,back;
    String listName;
    Spinner spinner;
     int dateID,timeID;
     Calendar calendar,startDate,endDate;
    private int year;
    private int month;
    private int day;
    private int reminderMinutes;
    private DatePickerDialog.OnDateSetListener myDateListener = new DatePickerDialog.OnDateSetListener() {
        @Override
        public void onDateSet(DatePicker arg0, int arg1, int arg2, int arg3) {
            showDate(arg1, arg2, arg3);
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.plan_shopping_date);
        initilizeViews();
        setClickListeners();
        setSpinner();
    }

    void initilizeViews(){
        Intent get = getIntent();
        listName = get.getStringExtra("list name");
        date = (TextView) findViewById(R.id.set_date);
        time = (TextView) findViewById(R.id.set_time);
        setPlan = (TextView) findViewById(R.id.set_plan);
        back = (TextView) findViewById(R.id.back);
        spinner = (Spinner) findViewById(R.id.spinner);
        utils.setFontAwesomeOnTextViews(this, new TextView[]{back});

        calendar = Calendar.getInstance();
        startDate = Calendar.getInstance();
        endDate = Calendar.getInstance();
        year = calendar.get(Calendar.YEAR);
        month = calendar.get(Calendar.MONTH);
        day = calendar.get(Calendar.DAY_OF_MONTH);
        int hour = calendar.get(Calendar.HOUR_OF_DAY);
        int minute = calendar.get(Calendar.MINUTE);

        startDate.set(year,month,day, hour, minute);
        endDate.set(year,month,day, hour +2, minute);
        updateDate();
        showDate(year, month, day);
        reminderMinutes = -1;
    }

    @Override
    protected void setClickListeners() {
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long id) {
                String item = adapterView.getItemAtPosition(position).toString();
                reminderMinutes = getReminderTime(item);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }

    void showDate(int y,int m,int d){
        if(dateID == R.id.set_date){
            startDate.set(y,m,d);
            updateDate();
        }
    }

    @SuppressWarnings("deprecation")
    public void setDate(View view) {
        dateID = view.getId();
        showDialog(999);
    }

    @Override
    protected Dialog onCreateDialog(int id) {
        // TODO Auto-generated method stub
        if (id == 999) {
            return new DatePickerDialog(this, myDateListener, year, month, day);
        }
        return null;
    }

    public void showTimePickerDialog(View v) {
        timeID = v.getId();
        DialogFragment newFragment = new TimePickerFragment();
        newFragment.show(getFragmentManager(), "timePicker");
    }

    private void updateDate(){
        Date startDateTime = startDate.getTime();
        Date endDateTime = endDate.getTime();
        SimpleDateFormat dateFormat = new SimpleDateFormat("EEE, dd/MMMM/yyyy");
        SimpleDateFormat timeFormat = new SimpleDateFormat("HH:mm a");
        date.setText(dateFormat.format(startDateTime));
        time.setText(timeFormat.format(startDateTime));
    }

    void setSpinner(){
        List<String> categories = new ArrayList<String>();
        categories.add("Set Reminder");
        categories.add("On time");
        categories.add("1 min before");
        categories.add("5 min before");
        categories.add("10 min before");
        categories.add("15 min before");
        categories.add("20 min before");
        categories.add("30 min before");
        categories.add("45 min before");
        categories.add("1 hour before");
        categories.add("2 hour before");
        categories.add("3 hour before");
        categories.add("12 hour before");
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, categories);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(dataAdapter);
    }

    int getReminderTime(String time){
        int minutes = -1;
        switch (time){

            case "Set Reminder":
                minutes = -1;
                break;
            case "On time":
                minutes = 0;
                break;
            case "1 min before":
                minutes = 1;
                break;
            case "5 min before":
                minutes = 5;
                break;
            case "10 min before":
                minutes = 10;
                break;
            case "15 min before":
                minutes = 15;
                break;
            case "20 min before":
                minutes = 20;
                break;
            case "30 min before":
                minutes = 30;
                break;
            case "45 min before":
                minutes = 45;
                break;
            case "1 hour before":
                minutes = 60;
                break;
            case "2 hour before":
                minutes = 120;
                break;
            case "3 hour before":
                minutes = 180;
                break;
            case "12 hour before":
                minutes = 720;
                break;
        }
        return minutes;
    }

    @SuppressLint("ValidFragment")
    public  class TimePickerFragment extends DialogFragment
            implements TimePickerDialog.OnTimeSetListener {

        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            // Use the current time as the default values for the picker
            final Calendar c = Calendar.getInstance();
            int hour = c.get(Calendar.HOUR_OF_DAY);
            int minute = c.get(Calendar.MINUTE);

            // Create a new instance of TimePickerDialog and return it
            return new TimePickerDialog(getActivity(), this, hour, minute,
                    DateFormat.is24HourFormat(getActivity()));
        }

        public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
            // Do something with the time chosen by the user
            if(timeID == R.id.set_time){
                startDate.set(Calendar.HOUR_OF_DAY,hourOfDay);
                startDate.set(Calendar.MINUTE,minute);
                endDate.set(Calendar.HOUR_OF_DAY,hourOfDay+2);
                endDate.set(Calendar.MINUTE,minute);
                updateDate();
            }
        }
    }
}

