package com.store.mobin.treehousestd.store.app.activities;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.widget.Toast;

import com.google.zxing.Result;
import com.store.mobin.treehousestd.store.app.utils.utils;

import me.dm7.barcodescanner.zxing.ZXingScannerView;

/**
 * Created by Treehouse.std on 3/30/2017.
 */

public class BarCodeReader extends BaseActivity implements ZXingScannerView.ResultHandler {
    String barCode;
    String listID;
    private ZXingScannerView mScannerView;

    @Override
    public void onCreate(Bundle state) {
        super.onCreate(state);
        mScannerView = new ZXingScannerView(this);   // Programmatically initialize the scanner view
        setContentView(mScannerView);                // Set the scanner view as the content view
        ActivityCompat.requestPermissions(this,
                new String[]{Manifest.permission.CAMERA},
                1);
        listID = getIntent().getStringExtra("listID");
    }

    @Override
    public void onResume() {
        super.onResume();
        mScannerView.setResultHandler(this); // Register ourselves as a handler for scan results.
        mScannerView.startCamera();          // Start camera on resume
    }

    @Override
    public void onPause() {
        super.onPause();
        mScannerView.stopCamera();           // Stop camera on pause
    }

    @Override
    public void handleResult(Result rawResult) {
        barCode = rawResult.getText();
        Intent backToList = new Intent(BarCodeReader.this, ListDetailActivity.class);
        backToList.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        backToList.putExtra("listID", listID);
        backToList.putExtra("code", barCode);
        utils.secondTime = true;
        setResult(RESULT_OK, backToList);
        finish();
        mScannerView.resumeCameraPreview(this);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case 1: {

                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.
                } else {

                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                    Toast.makeText(this, "Permission denied to use Camera", Toast.LENGTH_SHORT).show();
                }
                return;
            }

            // other 'case' lines to check for other
            // permissions this app might request
        }
    }
    @Override
    public void onBackPressed() {
        utils.secondTime = true;
        finish();
    }
}
