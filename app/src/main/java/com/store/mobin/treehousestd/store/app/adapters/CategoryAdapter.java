package com.store.mobin.treehousestd.store.app.adapters;

import android.support.annotation.NonNull;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.bignerdranch.expandablerecyclerview.ChildViewHolder;
import com.bignerdranch.expandablerecyclerview.ExpandableRecyclerAdapter;
import com.bignerdranch.expandablerecyclerview.ParentViewHolder;
import com.store.mobin.treehousestd.store.R;
import com.store.mobin.treehousestd.store.app.activities.CategoryItems;
import com.store.mobin.treehousestd.store.app.models.CategoriesAdmin;
import com.store.mobin.treehousestd.store.app.models.Item;
import com.store.mobin.treehousestd.store.app.utils.utils;

import java.util.ArrayList;

/**
 * Created by TreeHousestudio on 5/3/2017.
 */

public class CategoryAdapter extends ExpandableRecyclerAdapter<CategoriesAdmin, Item, CategoryAdapter.CategoryViewHolder, CategoryAdapter.ItemViewHolder> {


    /**
     * Primary constructor. Sets up {@link #mParentList} and {@link #mFlatItemList}.
     * <p>
     * Any changes to {@link #mParentList} should be made on the original instance, and notified via
     * {@link #notifyParentInserted(int)}
     * {@link #notifyParentRemoved(int)}
     * {@link #notifyParentChanged(int)}
     * {@link #notifyParentRangeInserted(int, int)}
     * {@link #notifyChildInserted(int, int)}
     * {@link #notifyChildRemoved(int, int)}
     * {@link #notifyChildChanged(int, int)}
     * methods and not the notify methods of RecyclerView.Adapter.
     *
     * @param parentList List of all parents to be displayed in the RecyclerView that this
     *                   adapter is linked to
     */
    public CategoryAdapter(@NonNull ArrayList<CategoriesAdmin> parentList) {
        super(parentList);
    }

    @NonNull
    @Override
    public CategoryViewHolder onCreateParentViewHolder(@NonNull ViewGroup parentViewGroup, int viewType) {
        View v = View.inflate(parentViewGroup.getContext(), R.layout.parent_category, null);

        return new CategoryViewHolder(v);
    }

    @NonNull
    @Override
    public ItemViewHolder onCreateChildViewHolder(@NonNull ViewGroup childViewGroup, int viewType) {
        View v = View.inflate(childViewGroup.getContext(), R.layout.child_item, null);
        return new ItemViewHolder(v);
    }

    @Override
    public void onBindParentViewHolder(@NonNull CategoryViewHolder parentViewHolder, int parentPosition, @NonNull CategoriesAdmin parent) {
        parentViewHolder.bind(parent);
    }

    @Override
    public void onBindChildViewHolder(@NonNull final ItemViewHolder childViewHolder, int parentPosition, int childPosition, @NonNull final Item child) {
        childViewHolder.bind(child);
        childViewHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                Toast.makeText(v.getContext(),child.getItemName(),Toast.LENGTH_SHORT).show();
                ((CategoryItems) v.getContext()).addTask(child.getItemName());
            }
        });

    }

    class ItemViewHolder extends ChildViewHolder {
        TextView itemName;

        /**
         * Default constructor.
         *
         * @param itemView The {@link View} being hosted in this ViewHolder
         */
        private ItemViewHolder(@NonNull View itemView) {
            super(itemView);
            itemName = (TextView) itemView.findViewById(R.id.itemName);
        }

        private void bind(Item item) {
            itemName.setText(item.getItemName());
        }
    }

    class CategoryViewHolder extends ParentViewHolder {
        TextView categoryName, expandArrow;

        /**
         * Default constructor.
         *
         * @param itemView The {@link View} being hosted in this ViewHolder
         */
        private CategoryViewHolder(@NonNull View itemView) {
            super(itemView);
            categoryName = (TextView) itemView.findViewById(R.id.categoryName);
            expandArrow = (TextView) itemView.findViewById(R.id.expandArrow);
            utils.setFontAwesomeOnTextViews(itemView.getContext(), new TextView[]{expandArrow});
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (isExpanded()) {
                        expandArrow.setText(v.getContext().getString(R.string.down_arrow));
                        collapseView();
                    } else {
                        expandArrow.setText(v.getContext().getString(R.string.up_arrow));
                        expandView();
                    }
                }
            });


        }

        //  return false will disable expansion on whole view
        @Override
        public boolean shouldItemViewClickToggleExpansion() {
            return false;
        }

        private void bind(CategoriesAdmin categoriesAdmin) {
            categoryName.setText(categoriesAdmin.getName());
        }
    }
}
