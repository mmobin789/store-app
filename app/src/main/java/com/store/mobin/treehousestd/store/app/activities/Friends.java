package com.store.mobin.treehousestd.store.app.activities;

import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.store.mobin.treehousestd.store.R;
import com.store.mobin.treehousestd.store.app.adapters.FriendsAdapter;
import com.store.mobin.treehousestd.store.app.models.Friend;
import com.store.mobin.treehousestd.store.app.models.Item;
import com.store.mobin.treehousestd.store.app.models.List;
import com.store.mobin.treehousestd.store.app.models.User;
import com.store.mobin.treehousestd.store.app.utils.utils;

import static com.store.mobin.treehousestd.store.app.models.Friend.friendsList;


public class Friends extends BaseActivity {

    TextView addFriend, addFriendBig, back, add, friendRequestCount, friendRequest, invite;
    DatabaseReference appUsers, addUserFriends;
    DatabaseReference syncListRef;

    Query userFriends;
    RecyclerView recyclerView;
    FriendsAdapter adapter;
    ValueEventListener getListToSend = new ValueEventListener() {
        @Override
        public void onDataChange(DataSnapshot dataSnapshot) {

            Friend.list2Friend = dataSnapshot.getValue(List.class);
            for(DataSnapshot ds:dataSnapshot.child("Items").getChildren())
            {
                Item item = ds.getValue(Item.class);
                Friend.items2Friend.add(item);

            }


            if (syncListRef != null)
                syncListRef.removeEventListener(getListToSend);


        }

        @Override
        public void onCancelled(DatabaseError databaseError) {

        }
    };
    ValueEventListener userFriendsEvent = new ValueEventListener() {
        @Override
        public void onDataChange(DataSnapshot dataSnapshot) {
            friendsList.clear();
            for (DataSnapshot ds : dataSnapshot.getChildren()) {
                Friend friend = ds.getValue(Friend.class);
                if (User.getUserEmail().equals(friend.getReceiverEmail())) {
                 //   friend.setFriendID(friend.getSenderID());
                    friendsList.add(friend);
                }
                if (User.getUserEmail().equals(friend.getSenderEmail())) {
                   // friend.setFriendID(friend.getReceiverID());
                    friendsList.add(friend);
                }
            }
            if (friendsList.size() > 0) {
                recyclerView.setVisibility(View.VISIBLE);
                invite.setVisibility(View.GONE);
                addFriendBig.setVisibility(View.GONE);
            }
            if (friendsList.isEmpty()) {
                invite.setVisibility(View.VISIBLE);
                addFriendBig.setVisibility(View.VISIBLE);
                recyclerView.setVisibility(View.GONE);
            }
            adapter.notifyDataSetChanged();

            //    friendRequestCount.setText(Friend.getFriendRequestCount());

        }

        @Override
        public void onCancelled(DatabaseError databaseError) {

        }
    };
    //   String phn = "";
    String email = "";
    ValueEventListener searchFriendsEvent = new ValueEventListener() {
        @Override
        public void onDataChange(DataSnapshot dataSnapshot) {
            Boolean found = false;

            for (DataSnapshot data : dataSnapshot.getChildren()) {
                User user = data.getValue(User.class);
                if (user.getEmail().equals(email)) {
                    //   StorageReference friendsPic = FirebaseStorage.getInstance().getReference(getString(R.string.app_name)).child("users").child("images/" + data.getKey());
                    Friend.id = data.getKey();
                    Friend.name = user.getUsername();
                    Friend.email = user.getEmail();
                    //   phn = user.getPhn();
                    found = true;
                    break;
                }


            }
            if (found) {
                add.setVisibility(View.VISIBLE);
                removeSearchFriendsEvent();
            } else {
                Toast.makeText(Friends.this, "No Friend Found with the corresponding address " + email, Toast.LENGTH_SHORT).show();
                removeSearchFriendsEvent();
            }


        }

        @Override
        public void onCancelled(DatabaseError databaseError) {

        }
    };

    void getSyncList() {
        syncListRef = FirebaseDatabase.getInstance().getReference(getString(R.string.app_name)).child("users").child(User.getUserID())
                .child("Lists").child(getIntent().getStringExtra("listID"));
        syncListRef.addValueEventListener(getListToSend);

    }

    private void removeUserFriendsEvent() {

        if (addUserFriends != null)
            addUserFriends.removeEventListener(userFriendsEvent);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.friends);
        initializeViews();
        setClickListeners();
    }

    @Override
    protected void onStart() {
        super.onStart();
       if(getIntent().hasExtra("listID"))
       {
           getSyncList();
       }
    }

    @Override
    protected void initializeViews() {
        back = (TextView) findViewById(R.id.back);
        addFriend = (TextView) findViewById(R.id.addContact);
        addFriendBig = (TextView) findViewById(R.id.addContactSingleton);
        friendRequest = (TextView) findViewById(R.id.friendRequests);
        friendRequestCount = (TextView) findViewById(R.id.friendRequestsCount);
        recyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        invite = (TextView) findViewById(R.id.invite);
        utils.setFontAwesomeOnTextViews(this, new TextView[]{back, addFriend, friendRequest, addFriendBig});

        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);
        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(recyclerView.getContext(),
                layoutManager.getOrientation());
        recyclerView.addItemDecoration(dividerItemDecoration);
        adapter = new FriendsAdapter(this, friendsList);
        recyclerView.setAdapter(adapter);


    }

    @Override
    protected void setClickListeners() {
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        addFriend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                loadDialog(v);
            }
        });
        addFriendBig.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                loadDialog(v);
            }
        });

    }

    @Override
    protected void onResume() {
        super.onResume();
        utils.secondTime = true;
        getFriends();
    }

    void getFriends() {

        userFriends = FirebaseDatabase.getInstance().getReference(getString(R.string.app_name)).child("Friends");
        userFriends.orderByChild("senderEmail").equals(User.getUserEmail());
        userFriends.addValueEventListener(userFriendsEvent);

    }

    private void loadDialog(View v) {

        final Dialog dialog = new Dialog(v.getContext());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.add_friends);
        dialog.setCancelable(false);
        dialog.setOnKeyListener(new DialogInterface.OnKeyListener() {
            @Override
            public boolean onKey(DialogInterface dialog, int keyCode, KeyEvent event) {

                if (keyCode == KeyEvent.KEYCODE_BACK) {
                    dialog.dismiss();
                }
                return true;
            }
        });
        //TextView title = (TextView) dialog.findViewById(R.id.dialogTitle);
        final EditText input = (EditText) dialog.findViewById(R.id.itemName);
        //status = (TextView) dialog.findViewById(R.id.status);
        TextView search = (TextView) dialog.findViewById(R.id.searchFriend);
        add = (TextView) dialog.findViewById(R.id.add);

        utils.setFontAwesomeOnTextViews(v.getContext(), new TextView[]{addFriend, search});
        search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (utils.isEmailValid(input)) {
                    searchFirebase(input.getText().toString().trim());
                } else Toast.makeText(v.getContext(), "Invalid email", Toast.LENGTH_SHORT).show();
            }
        });
        add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                recyclerView.setVisibility(View.VISIBLE);
                invite.setVisibility(View.GONE);
                addFriendBig.setVisibility(View.GONE);
                sendRequest();

                dialog.dismiss();
            }
        });


        dialog.show();


    }

    void sendRequest() {
        addUserFriends = FirebaseDatabase.getInstance().getReference(getString(R.string.app_name)).child("Friends");
        Friend friend = new Friend();
        friend.setSenderEmail(User.getUserEmail());
        friend.setReceiverEmail(Friend.email);
        friend.setSenderID(User.getUserID());
        friend.setReceiverID(Friend.id);
        friend.setSenderName(User.getUserName());
        friend.setReceiverName(Friend.name);
        friend.setRequestStatus(0);  // unaccepted
        friend.setRequestID("frid_" + utils.removeSpaces(friend.getSenderName() + friend.getReceiverName()) + utils.getRand());
        addUserFriends.child(friend.getRequestID()).setValue(friend);
        Toast.makeText(this, "Friend Request sent", Toast.LENGTH_SHORT).show();
        getFriends();


    }

    void removeSearchFriendsEvent() {
        if (appUsers != null)
            appUsers.removeEventListener(searchFriendsEvent);
        if (addUserFriends != null)
            addUserFriends.removeEventListener(userFriendsEvent);
    }

    private void searchFirebase(String email) {
        FirebaseDatabase f = FirebaseDatabase.getInstance();
        appUsers = f.getReference(getString(R.string.app_name)).child("users");
        this.email = email;
        appUsers.addValueEventListener(searchFriendsEvent);


    }


    @Override
    protected void onPause() {
        super.onPause();
        removeSearchFriendsEvent();
    }

    @Override
    protected void onStop() {
        super.onStop();
        removeSearchFriendsEvent();
    }

    @Override
    public void onBackPressed() {
        utils.secondTime = true;
        finish();
    }
}