package com.store.mobin.treehousestd.store.app.adapters;

import android.app.Activity;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.firebase.ui.storage.images.FirebaseImageLoader;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.store.mobin.treehousestd.store.R;
import com.store.mobin.treehousestd.store.app.models.Friend;
import com.store.mobin.treehousestd.store.app.models.Item;
import com.store.mobin.treehousestd.store.app.models.User;
import com.store.mobin.treehousestd.store.app.utils.utils;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by Treehouse.std on 3/14/2017.
 */

public class FriendsAdapter extends RecyclerView.Adapter<FriendsAdapter.AppViewHolder> {
    private ArrayList<Friend> friends;
    private Activity context;



    public FriendsAdapter(Activity context, ArrayList<Friend> friends) {
        this.friends = friends;
        this.context = context;

    }

    @Override
    public AppViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = View.inflate(parent.getContext(), R.layout.friends_adapter, null);

        return new AppViewHolder(view);
    }

    private void copyListToFriend(AppViewHolder holder) {
        DatabaseReference listRef = FirebaseDatabase.getInstance().getReference(context.getString(R.string.app_name))
                .child("users").child(friends.get(holder.getAdapterPosition()).getFriendID()).child("Lists").child(Friend.list2Friend.getId());

        listRef.setValue(Friend.list2Friend);
        DatabaseReference itemsRef = FirebaseDatabase.getInstance().getReference(context.getString(R.string.app_name))
                .child("users").child(friends.get(holder.getAdapterPosition()).getFriendID()).child("Lists").child(Friend.list2Friend.getId()).child("Items");


        for (int i = 0; i < Friend.items2Friend.size(); i++) {
            Item item2send = new Item();
            item2send.setId(Friend.items2Friend.get(i).getId());
            item2send.setQuantity(Friend.items2Friend.get(i).getQuantity());
            item2send.setItemName(Friend.items2Friend.get(i).getItemName());
            item2send.setListID(Friend.items2Friend.get(i).getListID());
            item2send.setStatus(Friend.items2Friend.get(i).getStatus());

            itemsRef.child(item2send.getId()).setValue(item2send);
        }


    }

    private void acceptRequestUpdateDB(AppViewHolder holder) {
        DatabaseReference acceptRef = FirebaseDatabase.getInstance().getReference(context.getString(R.string.app_name))
                .child("Friends").child(friends.get(holder.getAdapterPosition()).getRequestID());
        Friend req = new Friend();
        req.setRequestID(friends.get(holder.getAdapterPosition()).getRequestID());
        req.setSenderName(friends.get(holder.getAdapterPosition()).getSenderName());
        req.setSenderEmail(friends.get(holder.getAdapterPosition()).getSenderEmail());
        req.setSenderID(friends.get(holder.getAdapterPosition()).getSenderID());
        req.setReceiverID(friends.get(holder.getAdapterPosition()).getReceiverID());
        req.setReceiverName(friends.get(holder.getAdapterPosition()).getReceiverName());
        req.setReceiverEmail(friends.get(holder.getAdapterPosition()).getReceiverEmail());
        req.setRequestStatus(1);
        acceptRef.setValue(req);
        holder.requestStatus.setTextColor(Color.GREEN);
    }

    private void cancelRequestUpdateDB(AppViewHolder holder) {
        DatabaseReference cancelRef = FirebaseDatabase.getInstance().getReference(context.getString(R.string.app_name))
                .child("Friends").child(friends.get(holder.getAdapterPosition()).getRequestID());

        cancelRef.removeValue();

    }

    @Override
    public void onBindViewHolder(final AppViewHolder holder, int position) {


        if (friends.get(position).getRequestStatus() == 1) {
            holder.requestStatus.setTextColor(Color.GREEN);
            //      holder.requestStatus.setEnabled(false);
        } else {
            //    holder.requestStatus.setEnabled(true);
            holder.requestStatus.setTextColor(Color.GRAY);
        }   //
        if (User.getUserEmail().equals(friends.get(position).getReceiverEmail())) {
            Glide.with(holder.itemView.getContext()).using(new FirebaseImageLoader()).load(Friend.getFriendAvatarRef(friends.get(position).getSenderID())).into(holder.userPic);
            holder.name.setText(friends.get(position).getSenderName());
            holder.email.setText(friends.get(position).getSenderEmail());

            holder.requestStatus.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    acceptRequestUpdateDB(holder);

                }
            });

        friends.get(holder.getAdapterPosition()).setFriendID(friends.get(holder.getAdapterPosition()).getSenderID());

        } else {

            Glide.with(holder.itemView.getContext()).using(new FirebaseImageLoader()).load(Friend.getFriendAvatarRef(friends.get(position).getReceiverID())).into(holder.userPic);
            holder.name.setText(friends.get(position).getReceiverName());
            holder.email.setText(friends.get(position).getReceiverEmail());
            friends.get(holder.getAdapterPosition()).setFriendID(friends.get(holder.getAdapterPosition()).getReceiverID());

        }
        if (Friend.list2Friend != null) {
            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    copyListToFriend(holder);
                    Toast.makeText(v.getContext(), "List synced", Toast.LENGTH_LONG).show();
                    context.finish();

                }
            });
        }


    }

    @Override
    public int getItemCount() {
        return friends.size();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    class AppViewHolder extends RecyclerView.ViewHolder {
        CircleImageView userPic;
        TextView name, email, requestStatus, cancelRequest;


        AppViewHolder(View itemView) {
            super(itemView);
            name = (TextView) itemView.findViewById(R.id.name);
            email = (TextView) itemView.findViewById(R.id.email);
            userPic = (CircleImageView) itemView.findViewById(R.id.userPic);
            requestStatus = (TextView) itemView.findViewById(R.id.requestStatus);
            cancelRequest = (TextView) itemView.findViewById(R.id.cancelRequest);
            utils.setFontAwesomeOnTextViews(itemView.getContext(), new TextView[]{requestStatus, cancelRequest});


        }
    }
}



