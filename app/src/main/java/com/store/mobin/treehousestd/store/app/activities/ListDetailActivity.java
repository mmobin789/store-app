package com.store.mobin.treehousestd.store.app.activities;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Window;
import android.widget.EditText;
import android.widget.SearchView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.store.mobin.treehousestd.store.R;
import com.store.mobin.treehousestd.store.app.adapters.MyAdapter;
import com.store.mobin.treehousestd.store.app.models.Friend;
import com.store.mobin.treehousestd.store.app.models.Item;
import com.store.mobin.treehousestd.store.app.models.User;
import com.store.mobin.treehousestd.store.app.utils.utils;

import java.util.LinkedList;

import static com.store.mobin.treehousestd.store.app.utils.utils.setFontAwesomeOnTextViews;
import static com.store.mobin.treehousestd.store.app.utils.utils.validInput;

/**
 * Created by Treehouse.std on 3/5/2017.
 */

public class ListDetailActivity extends BaseActivity implements View.OnClickListener, SearchView.OnQueryTextListener {
    TextView back, addItem, friendRequestCount, friendRequest, barCodeScanner;
    RecyclerView recyclerView;
    MyAdapter recyclerAdapter;
    public static String listId;
    DatabaseReference databaseReference;
    SearchView searchView;
    public static LinkedList<Item> itemArrayList = new LinkedList<>();
    ValueEventListener valueEventListener = new ValueEventListener() {
        @Override
        public void onDataChange(DataSnapshot dataSnapshot) {
            itemArrayList.clear();
            for (DataSnapshot dataSnapshots : dataSnapshot.getChildren()) {
                Item savedItem = dataSnapshots.getValue(Item.class);
//                savedItem.setItemName(dataSnapshots.child("itemName").getValue() + "");
//                savedItem.setStatus(dataSnapshots.child("status").getValue() + "");
                savedItem.setListID(getIntent().getStringExtra("listID"));

                itemArrayList.add(savedItem);
            }
            if (recyclerAdapter == null) {
                recyclerAdapter = new MyAdapter(itemArrayList, true, ListDetailActivity.this);
                recyclerView.setAdapter(recyclerAdapter);
            } else {
                recyclerAdapter.notifyDataSetChanged();
            }
            closeProgressDialog();

        }

        @Override
        public void onCancelled(DatabaseError databaseError) {

        }
    };

    @Override
    protected void onResume() {

        if (utils.secondTime) {
            utils.secondTime = false;
            getTasks();
        }
        super.onResume();
    }
//    void showChatMessages() {
//        FirebaseListAdapter<ChatMessage> adapter;
//        adapter = new FirebaseListAdapter<ChatMessage>(this, ChatMessage.class, R.layout.chat_odd_adapter, FirebaseDatabase.getInstance().getReference()) {
//            @Override
//            protected void populateView(View v, ChatMessage model, int position) {
//
//
//
//                TextView messageText = (TextView) v.findViewById(R.id.chatLeft);
//                //   CircleImageView user = (CircleImageView) v.findViewById(R.id.userLeft);
//
//
//                CharSequence timeStamp = DateFormat.format("dd-MM-yyyy (HH:mm:ss a)",
//                        model.getMessageTime());
//                // Set their text
//                if (model.getMessageText() != null) {
//                    messageText.setText(model.getMessageUser() + "\n" + model.getMessageText() + "\n" + timeStamp, TextView.BufferType.SPANNABLE);
//                    Spannable s = (Spannable) messageText.getText();
//                    s.setSpan(new ForegroundColorSpan(Color.parseColor("#007678")), 0, model.getMessageUser().length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
//
//                    //  Glide.with(v.getContext()).using(new FirebaseImageLoader()).load(User.getUserPic()).into(user);
//                }
//
//
//            }
//        };
//        chatView.setAdapter(adapter);
//
//    }

    @Override
    public void onBackPressed() {
        utils.secondTime = true;
        super.onBackPressed();

    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (isNetworkConnected()) {
            setContentView(R.layout.list_detail);
            initializeViews();
            setClickListeners();
            getTasks();
        } else {
            Toast.makeText(this, "No Internet Found", Toast.LENGTH_SHORT).show();
        }
    }

    void addItemDialog() {
        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.add_task_dialog);
        final EditText itemName = (EditText) dialog.findViewById(R.id.itemName);
        itemName.setText("Item " + (itemArrayList.size() + 1) + "");
//        final EditText phn = (EditText) dialog.findViewById(R.id.phnNo);
//        final EditText charge = (EditText) dialog.findViewById(R.id.charge);
//        final TextView result = (TextView) dialog.findViewById(R.id.result);

//        result.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                showStatusPopUp(v, result);
//
//            }
//        });

        dialog.findViewById(R.id.add).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!validInput(itemName)) {
                    Toast.makeText(v.getContext(), itemName.getHint() + " Required", Toast.LENGTH_SHORT).show();
                } else {
                    addTask(itemName.getText().toString());
                    dialog.dismiss();
                }

                //  Toast.makeText(v.getContext(), getAddedProjectID(), Toast.LENGTH_SHORT).show();


            }
        });
        dialog.findViewById(R.id.cancel).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();

                //     Toast.makeText(v.getContext(),user.getUsername(),Toast.LENGTH_SHORT).show();
            }
        });
        dialog.show();


    }

    public void removeEventListener() {
        if (databaseReference != null)
            databaseReference.removeEventListener(valueEventListener);
    }

    @Override
    protected void onPause() {
        super.onPause();
        removeEventListener();
    }

    @Override
    protected void onStop() {
        super.onStop();
        removeEventListener();
    }

    private void getTasks() {

        FirebaseDatabase database = FirebaseDatabase.getInstance();
        databaseReference = database.getReference(getString(R.string.app_name)).child("users").child(User.getUserID()).child("Lists").child(listId).child("Items");
        showProgressDialog(ListDetailActivity.this, "Loading Items...");
        databaseReference.addValueEventListener(valueEventListener);
    }

    private void addTask(String itemName) {
        if (!utils.containSpecialCharacter(itemName)) {
            Item item = new Item();
            item.setItemName(itemName);
            item.setStatus("not done"); // default status
            item.setQuantity("1"); // default quantity
            item.setId("iid" + utils.removeSpaces(itemName) + utils.getRand());
            FirebaseDatabase database = FirebaseDatabase.getInstance();
            databaseReference = database.getReference(getString(R.string.app_name)).child("users").child(User.getUserID()).child("Lists").child(listId).child("Items");
            databaseReference.child(item.getId()).setValue(item);
            //getTasks();
        } else {
            Toast.makeText(this, "Item should not contains Special Characters", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    protected void initializeViews() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar_top);
        //   addContact = (TextView) toolbar.findViewById(R.id.addContact);
        //     settings = (TextView) toolbar.findViewById(R.id.settings);
        back = (TextView) toolbar.findViewById(R.id.back);

        searchView = (SearchView) findViewById(R.id.searchView);
        searchView.setOnQueryTextListener(this);
        //     itemsNav = (LinearLayout) findViewById(R.id.taskMenu);
        //   itemHeader = (TextView) findViewById(R.id.itemsHeader);
        recyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        //  chatView = (ListView) findViewById(R.id.chatV);
        addItem = (TextView) findViewById(R.id.addItem);
        categories = (TextView) findViewById(R.id.categoriesAdmin);
        // Button addItem = (Button) findViewById(R.id.addTask);
        //    chatInput = (EditText) findViewById(R.id.chatInput);
        //   send = (TextView) findViewById(R.id.chatSend);
        friendRequestCount = (TextView) findViewById(R.id.friendRequestsCount);
        friendRequest = (TextView) findViewById(R.id.friendRequests);
        barCodeScanner = (TextView) findViewById(R.id.bar_code_scanner);
        listId = getIntent().getStringExtra("listID");
        setFontAwesomeOnTextViews(this, new TextView[]{addItem, back, friendRequest, barCodeScanner, categories});
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);
        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(recyclerView.getContext(),
                layoutManager.getOrientation());
        recyclerView.addItemDecoration(dividerItemDecoration);
        friendRequestCount.setText(Friend.getFriendRequestCount());


    }

//    void initChat() {
//        if (FirebaseAuth.getInstance().getCurrentUser() != null)
//            FirebaseDatabase.getInstance().getReference().push().setValue(new ChatMessage(chatInput.getText().toString().trim(), FirebaseAuth.getInstance().getCurrentUser().getDisplayName()));
//        chatInput.setText(""); // clear it after chat sent
//    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 33 && resultCode == RESULT_OK) {
            if (data.getStringExtra("listID") != null) {
                listId = data.getStringExtra("listID");
                addTask(data.getStringExtra("code"));
            }
        }
        if(requestCode==333 && resultCode==RESULT_OK)
        {
            getTasks();
        }
    }

    TextView categories;

    @Override
    protected void setClickListeners() {
        back.setOnClickListener(this);
        //   settings.setOnClickListener(this);
//        addContact.setOnClickListener(this);
        addItem.setOnClickListener(this);
        friendRequest.setOnClickListener(this);
        categories.setOnClickListener(this);
        barCodeScanner.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent barCodeIntent = new Intent(ListDetailActivity.this, BarCodeReader.class);
                barCodeIntent.putExtra("listID", listId);
                startActivityForResult(barCodeIntent, 33);
            }
        });
        //   send.setOnClickListener(this);
//        chatInput.addTextChangedListener(new TextWatcher() {
//            @Override
//            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
//                chatView.setVisibility(View.VISIBLE);
//            }
//
//            @Override
//            public void onTextChanged(CharSequence s, int start, int before, int count) {
//
//                chatView.setVisibility(View.VISIBLE);
//                //            itemHeader.setVisibility(View.VISIBLE);
//                //              itemsNav.setVisibility(View.GONE);
//
//            }
//
//            @Override
//            public void afterTextChanged(Editable s) {
//                if (s.toString().isEmpty())
//                    chatView.setVisibility(View.GONE);
//                //     itemHeader.setVisibility(View.GONE);
//                //     itemsNav.setVisibility(View.VISIBLE);
//
//
//            }
//        });
    }

    void openContacts() {
        startActivity(new Intent(this, AppUsers.class));
    }

    void openSettings() {
        startActivity(new Intent(this, SettingsActivity.class));
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
//            case R.id.addContact:
//                openContacts();
//                break;
            case R.id.friendRequests:
                startActivity(new Intent(v.getContext(), Friends.class));
                break;
            case R.id.back:
                onBackPressed();
                break;
            case R.id.addItem:
                addItemDialog();
                break;
            case R.id.categoriesAdmin:
                startActivityForResult(new Intent(v.getContext(), CategoryItems.class),333);
                break;
//            case R.id.chatSend:
//                initChat();
//                break;


        }

    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        if (newText.length() > 0) {
            recyclerAdapter.filter(newText);
            //    recyclerView.invalidate();

            return true;
        } else {
            getTasks();
            return true;
        }


    }
}
