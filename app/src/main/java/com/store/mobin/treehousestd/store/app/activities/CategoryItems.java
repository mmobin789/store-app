package com.store.mobin.treehousestd.store.app.activities;

import android.app.Dialog;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.Window;
import android.widget.TextView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.store.mobin.treehousestd.store.R;
import com.store.mobin.treehousestd.store.app.adapters.CategoryAdapter;
import com.store.mobin.treehousestd.store.app.models.CategoriesAdmin;
import com.store.mobin.treehousestd.store.app.models.Category;
import com.store.mobin.treehousestd.store.app.models.Item;
import com.store.mobin.treehousestd.store.app.models.User;
import com.store.mobin.treehousestd.store.app.utils.utils;

import java.util.ArrayList;

import static com.store.mobin.treehousestd.store.app.activities.ListDetailActivity.listId;

public class CategoryItems extends BaseActivity {
    RecyclerView recyclerView;
    DatabaseReference databaseReference;
    TextView back;


    //  ArrayList<Item> items = new ArrayList<>();
    ArrayList<CategoriesAdmin> categories = new ArrayList<>();
    ValueEventListener getCategories = new ValueEventListener() {
        @Override
        public void onDataChange(DataSnapshot dataSnapshot) {
            Category category;

            for (DataSnapshot dataSnapshots : dataSnapshot.getChildren()) {
                category = dataSnapshots.getValue(Category.class);
                ArrayList<Item> items = new ArrayList<>();
                for (DataSnapshot itemShot : dataSnapshot.child(category.getId()).child("Items").getChildren()) {

                    Item item = new Item();
                    item.setItemName(itemShot.child("itemName").getValue() + "");
                    item.setId(itemShot.child("id").getValue() + "");
                    items.add(item);
                }

                category.setItems(items);

                CategoriesAdmin categorized = new CategoriesAdmin();
                categorized.setName(category.getName());
                categorized.setId(category.getId());
                categorized.setItems(category.getItems());

                categories.add(categorized);


            }
            if (categoryAdapter == null) {
                categoryAdapter = new CategoryAdapter(categories);
                recyclerView.setAdapter(categoryAdapter);
            } else {
                categoryAdapter.notifyDataSetChanged();
            }
        }


        @Override
        public void onCancelled(DatabaseError databaseError) {

        }
    };

    public void addTask(String itemName) {
        Item item = new Item();
        item.setItemName(itemName);
        item.setStatus("not done"); // default status
        item.setQuantity("1"); // default quantity
        item.setId("iid" + utils.removeSpaces(itemName) + utils.getRand());
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        databaseReference = database.getReference(getString(R.string.app_name)).child("users").child(User.getUserID()).child("Lists").child(listId).child("Items");
        databaseReference.child(item.getId()).setValue(item);
        showItemAdded(itemName);
        setResult(RESULT_OK);
    }

    void showItemAdded(String string) {

        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.ok_dialog);
        TextView msg = (TextView) dialog.findViewById(R.id.msg);
        msg.setText(string + " added successfully"+"");
        dialog.findViewById(R.id.ok).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.show();

    }

    private void closeFirebaseDB() {
        if (databaseReference != null)
            databaseReference.removeEventListener(getCategories);
    }

    @Override
    protected void onPause() {
        super.onPause();
        closeFirebaseDB();
    }

    @Override
    protected void onStop() {
        super.onStop();
        closeFirebaseDB();
    }

    @Override
    protected void onStart() {
        super.onStart();
        getCategories();
    }

    private void getCategories() {
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        databaseReference = database.getReference(getString(R.string.app_name)).child("Categories");
        databaseReference.addValueEventListener(getCategories);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_category);
        initializeViews();
        setClickListeners();
        setCategories();

    }

    @Override
    protected void initializeViews() {
        back = (TextView) findViewById(R.id.back);
        utils.setFontAwesomeOnTextViews(this, new TextView[]{back});
    }

    @Override
    protected void setClickListeners() {
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }

    CategoryAdapter categoryAdapter;

    void setCategories() {
        recyclerView = (RecyclerView) findViewById(R.id.categoryView);

        recyclerView.setLayoutManager(new LinearLayoutManager(this));


    }


}
