package com.store.mobin.treehousestd.store.app.activities;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.store.mobin.treehousestd.store.R;
import com.store.mobin.treehousestd.store.app.models.User;

import java.util.ArrayList;

import static com.store.mobin.treehousestd.store.app.utils.utils.setFontAwesomeOnTextViews;

/**
 * Created by Treehouse.std on 3/14/2017.
 */

public class AppUsers extends BaseActivity {
    RecyclerView recyclerView;
    TextView back;
    // FriendsAdapter usersAdapter;
    FirebaseStorage firebaseStorage;
    long when = System.currentTimeMillis();
    ArrayList<User> userArrayList = new ArrayList<>();

    DatabaseReference databaseReference;

    ValueEventListener phnNosFromFirebase = new ValueEventListener() {
        @Override
        public void onDataChange(DataSnapshot dataSnapshot) {
            for (DataSnapshot dataSnapshots : dataSnapshot.getChildren()) {
                //   String userID = (String) dataSnapshots.getValue();
                User users = dataSnapshots.getValue(User.class);
                StorageReference imageRefs = firebaseStorage.getReference(getString(R.string.app_name)).child("users").child("images/" + dataSnapshots.getKey());
                users.setUserAvatar(imageRefs);
                userArrayList.add(users);

            }
            new fetchAppUsers().execute();


        }

        @Override
        public void onCancelled(DatabaseError databaseError) {

        }
    };

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        firebaseStorage = FirebaseStorage.getInstance();
        setContentView(R.layout.friends);
        initializeViews();
        if (Build.VERSION.SDK_INT > 21) {
            if (ContextCompat.checkSelfPermission(this,
                    Manifest.permission.READ_CONTACTS)
                    != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_CONTACTS}, 33);


            } else {

                getDBUsers();


            }
        } else {

            getDBUsers();
        }

    }

    private void getDBUsers() {
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        databaseReference = database.getReference("Taskush").child("users");
        databaseReference.addValueEventListener(phnNosFromFirebase);
    }

    ArrayList<String> getAllContacts() {
        ArrayList<String> phnNos = new ArrayList<>();

        Cursor cursor = getContentResolver().query(ContactsContract.Contacts.CONTENT_URI, null, null, null, null);
        if (cursor != null) {
            Integer contactsCount = cursor.getCount();
            // get how many contacts you have in your contacts list
            if (contactsCount > 0) {
                while (cursor.moveToNext()) {
                    String id = cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts._ID));
                    String contactName = cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME));
                    if (Integer.parseInt(cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts.HAS_PHONE_NUMBER))) > 0) {
                        //the below cursor will give you details for multiple contacts
                        Cursor pCursor = getContentResolver().query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null,
                                ContactsContract.CommonDataKinds.Phone.CONTACT_ID + " = ?",
                                new String[]{id}, null);
                        // continue till this cursor reaches to all phone numbers which are associated with a contact in the contact list
                        if (pCursor != null) {
                            while (pCursor.moveToNext()) {
                                int phoneType = pCursor.getInt(pCursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.TYPE));
                                //String isStarred 		= pCur.getString(pCur.getColumnIndex(ContactsContract.CommonDataKinds.Phone.STARRED));
                                String phoneNo = pCursor.getString(pCursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
                                phnNos.add(phoneNo);
                                //you will get all phone numbers according to it's type as below switch case.
                                //Logs.e will print the phone number along with the name in DDMS. you can use these details where ever you want.
                                switch (phoneType) {
                                    case ContactsContract.CommonDataKinds.Phone.TYPE_MOBILE:
                                        Log.e(contactName + ": TYPE_MOBILE", " " + phoneNo);
                                        break;
                                    case ContactsContract.CommonDataKinds.Phone.TYPE_HOME:
                                        Log.e(contactName + ": TYPE_HOME", " " + phoneNo);
                                        break;
                                    case ContactsContract.CommonDataKinds.Phone.TYPE_WORK:
                                        Log.e(contactName + ": TYPE_WORK", " " + phoneNo);
                                        break;
                                    case ContactsContract.CommonDataKinds.Phone.TYPE_WORK_MOBILE:
                                        Log.e(contactName + ": TYPE_WORK_MOBILE", " " + phoneNo);
                                        break;
                                    case ContactsContract.CommonDataKinds.Phone.TYPE_OTHER:
                                        Log.e(contactName + ": TYPE_OTHER", " " + phoneNo);
                                        break;
                                    default:
                                        break;
                                }
                            }
                            pCursor.close();
                        }
                    }
                }
                cursor.close();
            }
        }
        return phnNos;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            getDBUsers();

        } else {
            Toast.makeText(this, permissions[0] + " Permission Denied", Toast.LENGTH_LONG).show();
            finish();
        }
    }


    @Override
    protected void onPause() {
        super.onPause();
        if (databaseReference != null)
            databaseReference.removeEventListener(phnNosFromFirebase);
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (databaseReference != null)
            databaseReference.removeEventListener(phnNosFromFirebase);
    }


    public void back(View v) {
        onBackPressed();
    }

    @Override
    protected void initializeViews() {
        //  Toolbar toolbar = (Toolbar)findViewById(R.id.toolbar_top);
        back = (TextView) findViewById(R.id.back);
        setFontAwesomeOnTextViews(this, new TextView[]{back});
        recyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);
        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(recyclerView.getContext(), layoutManager.getOrientation());
        recyclerView.addItemDecoration(dividerItemDecoration);


    }

    class fetchAppUsers extends AsyncTask<Void, Void, ArrayList<User>> {

        ProgressDialog progressDialog;


        @Override
        protected void onPreExecute() {
            progressDialog = new ProgressDialog(AppUsers.this);
            progressDialog.setMessage("Fetching FriendRequest...");
            progressDialog.setIndeterminate(true);
            progressDialog.setCancelable(false);
            progressDialog.show();
        }

        @Override
        protected ArrayList<User> doInBackground(Void... params) {

            int d = 0;
            ArrayList<String> phnNos = getAllContacts();

            ArrayList<User> users = new ArrayList<>();


            for (int i = 0; i < phnNos.size(); i++) {

                if (phnNos.get(i).contains(userArrayList.get(d).getPhn())) {


                    User user = new User();
                    user.setPhn(phnNos.get(i));
                    user.setEmail(userArrayList.get(d).getEmail());
                    user.setUsername(userArrayList.get(d).getUsername());
                    user.setUserAvatar(userArrayList.get(d).getUserAvatar());
                    users.add(user);

                    if (d == userArrayList.size() - 1) {
                        d = 0;
                    } else {
                        d++;
                    }
                }
            }
            return users;
        }

        @Override
        protected void onPostExecute(ArrayList<User> users) {
//            usersAdapter = new FriendsAdapter(users);
//            recyclerView.setAdapter(usersAdapter);
//            progressDialog.dismiss();


        }


    }

}
