package com.store.mobin.treehousestd.store.app.services;

import android.app.IntentService;
import android.content.Intent;
import android.os.IBinder;
import android.support.annotation.Nullable;

import com.store.mobin.treehousestd.store.app.activities.ListsActivity;
import com.store.mobin.treehousestd.store.app.utils.utils;

/**
 * Created by Treehouse.std on 3/30/2017.
 */


/**
 * Created by Treehouse.std on 3/17/2017.
 */

public class AlarmService extends IntentService {



    public AlarmService() {
        super("AlarmService");
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        utils.sendNotification(this, intent.getStringExtra("reminder"),intent.getIntExtra("code",0), ListsActivity.class);

    }
}