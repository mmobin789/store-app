package com.store.mobin.treehousestd.store.app.models;

import com.google.firebase.storage.StorageReference;

import java.util.ArrayList;

/**
 * Created by Treehouse.std on 3/5/2017.
 */

public class Item {
    public static Item item2move;
    public static Boolean move = false;
    public static ArrayList<Item> clickedListItems = new ArrayList<>();
    private String itemName;
    private StorageReference itemURL;
    private String id;
    private String listID;
    private String status;
    private String quantity;



    public String getQuantity() {
        return quantity;
    }

    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }



    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }


    public String getListID() {
        return listID;
    }

    public void setListID(String listID) {
        this.listID = listID;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public StorageReference getItemURL() {
        return itemURL;
    }

    public void setItemURL(StorageReference itemURL) {
        this.itemURL = itemURL;
    }

    public String getItemName() {
        return itemName;
    }

    public void setItemName(String itemName) {
        this.itemName = itemName;
    }
}
