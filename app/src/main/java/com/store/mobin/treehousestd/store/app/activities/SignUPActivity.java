package com.store.mobin.treehousestd.store.app.activities;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.View;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.UserProfileChangeRequest;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.mvc.imagepicker.ImagePicker;
import com.store.mobin.treehousestd.store.R;
import com.store.mobin.treehousestd.store.app.models.User;
import com.store.mobin.treehousestd.store.app.utils.utils;

import java.io.ByteArrayOutputStream;

import de.hdodenhof.circleimageview.CircleImageView;

import static com.store.mobin.treehousestd.store.app.models.User.sharedPreferences;
import static com.store.mobin.treehousestd.store.app.utils.utils.validInput;


/**
 * Created by Treehouse.std on 3/6/2017.
 */

public class SignUPActivity extends BaseActivity {
    CircleImageView profilePic;
    EditText userName, password, email, phn;
    TextView submit;
    CheckBox terms;
    Bitmap userPic;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.signup);
        initializeViews();
        setClickListeners();

    }

    void disableView(View view) {
        view.setEnabled(false);
    }

    public void backPressed(View view) {
        onBackPressed();
    }

    @Override
    protected void initializeViews() {
        //     name = (EditText) findViewById(R.id.name);
        profilePic = (CircleImageView) findViewById(R.id.profile_image);
        userName = (EditText) findViewById(R.id.userName);
        password = (EditText) findViewById(R.id.password);
        email = (EditText) findViewById(R.id.email);
        //   gender = (EditText) findViewById(R.id.sex);
        terms = (CheckBox) findViewById(R.id.terms);
        phn = (EditText) findViewById(R.id.phn);
        submit = (TextView) findViewById(R.id.submit);
        TextView back = (TextView) findViewById(R.id.back);
        utils.setFontAwesomeOnTextViews(this, new TextView[]{back});
        utils.setFontAwesomeOnTextViews(this, new TextView[]{back});
    }

    @Override
    protected void setClickListeners() {
        terms.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    submit.setEnabled(true);
                    submit.setTextColor(Color.parseColor("#c3dcdc"));
                } else {
                    disableView(submit);
                    submit.setTextColor(Color.parseColor("#007678"));

                }
            }
        });
        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (!validInput(userName) ||
                        !validInput(password) || !validInput(email) || !validInput(phn)) {
                    Toast.makeText(v.getContext(), "Input Required", Toast.LENGTH_SHORT).show();

                } else if (userPic == null) {
                    Toast.makeText(v.getContext(), "Avatar Required", Toast.LENGTH_SHORT).show();
                } else {
                    if (isNetworkConnected())
                        signUp();
                    else
                        Toast.makeText(v.getContext(), "No Internet Found", Toast.LENGTH_SHORT).show();

                }
            }
        });
    }

    public void addAvatar(View v) {
        ImagePicker.pickImage(this);
        ImagePicker.setMinQuality(512, 512);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        userPic = ImagePicker.getImageFromResult(this, requestCode, resultCode, data);
        if (userPic != null) {
            profilePic.setImageBitmap(userPic);
            //   sharedPreferences(this).edit().putString("avatar", data.getData().getPath()).apply();
        }
    }

    void saveAvatar2Firebase(final String uID) {
        FirebaseStorage firebaseStorage = FirebaseStorage.getInstance();
        //    File imageFile = new File(path);
        //  Uri fileUri = Uri.fromFile(imageFile);
        StorageReference storageReference = firebaseStorage.getReference(getString(R.string.app_name)).child("users").child("images/" + uID);
        // InputStream inputStream = new FileInputStream(imageFile);
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        userPic.compress(Bitmap.CompressFormat.PNG, 100, baos);
        byte[] data = baos.toByteArray();
        UploadTask uploadUserPic = storageReference.putBytes(data);
        uploadUserPic.addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
            @Override
            public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                Log.i("uploadSuccess", taskSnapshot.getDownloadUrl() + "");


            }

        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                //   Toast.makeText(SignUPActivity.this, e.getMessage(), Toast.LENGTH_LONG).show();
                Log.e("uploadFailedtryAgain", e.getMessage());
                saveAvatar2Firebase(uID);
            }
        });


    }


    //ArrayList<User> users = new ArrayList<>();
    void saveONFirebaseAndSetProfile(Task<AuthResult> task, User user) {
        // user.setName(name.getText().toString());
        user.setUsername(userName.getText().toString());
        // user.setGender(gender.getText().toString());
        user.setPhn(phn.getText().toString());

        DatabaseReference databaseReference = FirebaseDatabase.getInstance().getReference(getString(R.string.app_name)).child("users");
        databaseReference.child(task.getResult().getUser().getUid()).setValue(user);
        UserProfileChangeRequest userProfileChangeRequest = new UserProfileChangeRequest.Builder().setDisplayName(user.getUsername()).build();

        saveAvatar2Firebase(task.getResult().getUser().getUid());

        sharedPreferences(this).edit().putString("email", task.getResult().getUser().getEmail()).apply(); // save offline
        sharedPreferences(this).edit().putString("password", user.getPassword()).apply();
        task.getResult().getUser().updateProfile(userProfileChangeRequest).addOnCompleteListener(this, new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                if (task.isSuccessful()) {
                    Log.i("ProfileSet", "User profile updated!!");
                    Intent openApp = new Intent(SignUPActivity.this, ListsActivity.class);
                    openApp.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    openApp.putExtra("txt", "Signed up and logged in");
                    startActivity(openApp);
                    finish();
                }

            }
        });

    }

    void signUp() {
        final User user = new User();
        user.setEmail(email.getText().toString());
        user.setPassword(password.getText().toString());
        showProgressDialog(this, "Signing Up...");

        FirebaseAuth.getInstance().createUserWithEmailAndPassword(user.getEmail(), user.getPassword()).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                if (task.isSuccessful()) {
                    closeProgressDialog();
                    Toast.makeText(SignUPActivity.this, "Account Created Successfully.", Toast.LENGTH_SHORT).show();

                    saveONFirebaseAndSetProfile(task, user);


                } else {
                    closeProgressDialog();
                    Toast.makeText(SignUPActivity.this, "Failed " + task.getException().getMessage(), Toast.LENGTH_LONG).show();
                }


            }
        });
        //  users.add(user);
        //databaseReference.push().setValue(user);


    }
}
