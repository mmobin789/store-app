package com.store.mobin.treehousestd.store.app.models;

/**
 * Created by Abdul Ghani on 3/28/2017.
 */

public class Reminder {
    private String reminder = "";
    private Boolean reminderCheck;
    private int day, year, month, hour, minute;
    private String amOrPm;
    private int alarmCode;
    private String beforeReminder;

    public int getAlarmCode() {
        return alarmCode;
    }

    public void setAlarmCode(int alarmCode) {
        this.alarmCode = alarmCode;
    }

    public String getAmOrPm() {
        return amOrPm;
    }

    public void setAmOrPm(String amOrPm) {
        this.amOrPm = amOrPm;
    }

    public int getDay() {
        return day;
    }

    public void setDay(int day) {
        this.day = day;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public int getMonth() {
        return month;
    }

    public void setMonth(int month) {
        this.month = month;
    }

    public int getHour() {
        return hour;
    }

    public void setHour(int hour) {
        this.hour = hour;
    }

    public int getMinute() {
        return minute;
    }

    public void setMinute(int minute) {
        this.minute = minute;
    }

    public Boolean getReminderCheck() {
        return reminderCheck;
    }

    public void setReminderCheck(Boolean reminderCheck) {
        this.reminderCheck = reminderCheck;
    }

    public String getReminder() {
        return reminder;
    }

    public void setReminder(String reminder) {
        this.reminder = reminder;
    }

    public String getBeforeReminder() {
        return beforeReminder;
    }

    public void setBeforeReminder(String beforeReminder) {
        this.beforeReminder = beforeReminder;
    }
}
