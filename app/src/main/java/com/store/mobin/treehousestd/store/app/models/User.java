package com.store.mobin.treehousestd.store.app.models;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

/**
 * Created by Treehouse.std on 3/19/2017.
 */

public class User {
    private StorageReference userAvatar;
    private String username;
    private String email;
    private String name;
    private String password;
    private String phn;
    private float rating;

    public User() {
        // Default constructor required for calls to DataSnapshot.getValue(User.class)
    }


    public static SharedPreferences sharedPreferences(Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context);
    }

    @NonNull
    public static StorageReference getUserPic() {
        FirebaseStorage firebaseStorage = FirebaseStorage.getInstance();
        return firebaseStorage.getReference("Your Shopping App").child("users").child("images/" + getUserID());
    }

    @NonNull
    public static String getUserID() {
        FirebaseAuth firebaseAuth = FirebaseAuth.getInstance();
        if (firebaseAuth.getCurrentUser() != null)
            return firebaseAuth.getCurrentUser().getUid();
        else return "null";
    }

    public static String getUserEmail() {
        FirebaseAuth firebaseAuth = FirebaseAuth.getInstance();
        if (firebaseAuth.getCurrentUser() != null)
            return firebaseAuth.getCurrentUser().getEmail();
        else return "null";

    }

    public static String getUserName() {
        FirebaseAuth firebaseAuth = FirebaseAuth.getInstance();
        if (firebaseAuth.getCurrentUser() != null)
            return firebaseAuth.getCurrentUser().getDisplayName();
        else return "null";
    }

    public StorageReference getUserAvatar() {
        return userAvatar;
    }

    public void setUserAvatar(StorageReference userAvatar) {
        this.userAvatar = userAvatar;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhn() {
        return phn;
    }

    public void setPhn(String phn) {
        this.phn = phn;
    }

    public String getUsername() {

        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public float getRating() {
        return rating;
    }

    public void setRating(float rating) {
        this.rating = rating;
    }
}
