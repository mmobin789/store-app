package com.store.mobin.treehousestd.store.app.models;

import com.google.firebase.storage.StorageReference;

/**
 * Created by Treehouse.std on 3/4/2017.
 */

public class List {


    private String name;
    private String id;
    private String description;
    private StorageReference avatar;
    private boolean favorite;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


    public StorageReference getAvatar() {
        return avatar;
    }

    public void setAvatar(StorageReference avatar) {
        this.avatar = avatar;
    }

    public boolean isFavorite() {
        return favorite;
    }

    public void setFavorite(boolean favorite) {
        this.favorite = favorite;
    }
}
