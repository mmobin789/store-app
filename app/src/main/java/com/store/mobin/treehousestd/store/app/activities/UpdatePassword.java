package com.store.mobin.treehousestd.store.app.activities;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.store.mobin.treehousestd.store.R;
import com.store.mobin.treehousestd.store.app.utils.utils;

import static com.store.mobin.treehousestd.store.app.models.User.sharedPreferences;

/**
 * Created by Treehouse.std on 3/27/2017.
 */

public class UpdatePassword extends BaseActivity {



    EditText oldPassword, newPassword, confirmPassword;
    TextView update, back;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.updatepassword);
        initilizeViews();
        setClickListeners();
    }

    void initilizeViews() {
        oldPassword = (EditText) findViewById(R.id.old_password);
        newPassword = (EditText) findViewById(R.id.new_password);
        confirmPassword = (EditText) findViewById(R.id.confirm_password);
        update = (TextView) findViewById(R.id.update_password);
        back = (TextView) findViewById(R.id.back);
        utils.setFontAwesomeOnTextViews(this, new TextView[]{back});
    }

    @Override
    protected void setClickListeners() {
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        update.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                changePassword();
            }
        });
    }

    void updatePassword(final String Password) {
        if (utils.validInput(newPassword)) {
            final FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
            if (user != null) {
                user.updatePassword(Password).addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        if (task.isSuccessful()) {
                            Toast.makeText(UpdatePassword.this, "Password Updated", Toast.LENGTH_SHORT).show();
                            sharedPreferences(UpdatePassword.this).edit().putString("email", user.getEmail()).apply();
                            sharedPreferences(UpdatePassword.this).edit().putString("password", Password).apply();
                        } else {
                            Toast.makeText(UpdatePassword.this, "" + task.getException().getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    }
                });
            } else {
                Toast.makeText(this, "New Password Should have at least 6 characters", Toast.LENGTH_SHORT).show();
            }
        }
    }

    void changePassword() {
        //if (utils.validInput(oldPassword)) {
        checkOldPassword(oldPassword.getText().toString());
        //}
    }

    void checkOldPassword(String oldPassword) {
        String passwordTxt = sharedPreferences(this).getString("password", "No");
        if (!passwordTxt.equals("No")) {
            if (passwordTxt.equals(oldPassword)) {
                if (newPassword.getText().toString().equals(confirmPassword.getText().toString())) {
                    updatePassword(newPassword.getText().toString());
                } else {
                    Toast.makeText(this, "New and Confirm Password should be same", Toast.LENGTH_SHORT).show();
                }
            } else {
                Toast.makeText(this, "Old Password is Incorrect", Toast.LENGTH_SHORT).show();
            }
        }else{
            Toast.makeText(this, "Old Password is Incorrect", Toast.LENGTH_SHORT).show();
        }
    }
    @Override
    public void onBackPressed() {
        utils.secondTime = true;
        finish();
    }

    @Override
    protected void onResume() {
        super.onResume();
        utils.secondTime = true;
    }
}

