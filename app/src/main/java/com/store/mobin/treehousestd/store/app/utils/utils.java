package com.store.mobin.treehousestd.store.app.utils;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.EditText;
import android.widget.TextView;

import com.store.mobin.treehousestd.store.R;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Date;
import java.util.Random;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static android.content.Context.NOTIFICATION_SERVICE;

/**
 * Created by Treehouse.std on 3/19/2017.
 */

public class utils {

    public static Boolean secondTime = false;
    public static String listID = "";

    public static String replaceAllSpecialCharsAndSpaces(String s) {
        return s.replaceAll("[^a-zA-Z0-9_-]", "");
    }
    public static boolean containSpecialCharacter(String s){
        Pattern p = Pattern.compile("[^a-z0-9 ]", Pattern.CASE_INSENSITIVE);
        Matcher m = p.matcher(s);
        return m.find();
    }

    public static String urlEncode(String s) {
        try {
            return URLEncoder.encode(s, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            Log.wtf("utf", "UTF-8 should always be supported", e);
            throw new RuntimeException("URLEncoder.encode() failed for " + s);
        }
    }

    //public static Boolean chepi = false;
    public static String getFacebookKeyHash(Context context) {
        String keyhash = "";

        try {
            @SuppressLint("PackageManagerGetSignatures") PackageInfo info = context.getPackageManager().getPackageInfo(
                    context.getPackageName(),
                    PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                keyhash = Base64.encodeToString(md.digest(), Base64.DEFAULT);
                Log.d("KeyHash:", keyhash);
            }
        } catch (PackageManager.NameNotFoundException | NoSuchAlgorithmException ignored) {

        }
        return keyhash;
    }


    public static void noNetDialog(final Activity activity) {
        final Dialog dialog = new Dialog(activity);
        dialog.setCancelable(false);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.internet);
        TextView cancel = (TextView) dialog.findViewById(R.id.cancel);
        cancel.setTextColor(Color.YELLOW);
        setFontAwesomeOnTextViews(activity, new TextView[]{cancel});
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                activity.finish();
            }
        });
        dialog.show();

    }

    public static void sendNotification(Context c, String msg,int code, Class classToLaunch) {
        Intent intent = new Intent(c, classToLaunch);
        PendingIntent pIntent = PendingIntent.getActivity(c, 0, intent, PendingIntent.FLAG_CANCEL_CURRENT);
        Notification n = new Notification.Builder(c)
                .setContentTitle(c.getString(R.string.app_name))
                .setContentText(msg)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setContentIntent(pIntent)
                .setAutoCancel(true)
                .build();
        NotificationManager notificationManager =
                (NotificationManager) c.getSystemService(NOTIFICATION_SERVICE);
        notificationManager.notify(code, n);

    }

    @NonNull
    public static Boolean isEmailValid(EditText editText) {
        String emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";
        return editText.getText().toString().trim().matches(emailPattern);
    }

    @NonNull
    public static Boolean validInput(EditText editText) {
        if (editText.getText().toString().trim().isEmpty()) {
            editText.setError(editText.getHint());
            return false;
        } else {
            return true;
        }
    }

    //  public static int sign_in_RequestCode = 33;

    public static int getRand(int max, int min) {
        Random random = new Random();
        return random.nextInt((max - min) + 1) + min;
    }

    public static int getRand() {
        return (int) ((new Date().getTime() / 1000L) % Integer.MAX_VALUE);
    }

    public static String removeSpaces(String s) {
        return s.replaceAll("\\s+", "");
    }

    public static void setFontAwesomeOnTextViews(Context context, TextView[] textViews) {
        //Typeface fontAwesome = Typeface.createFromAsset(getAssets(),"fontAwesome.ttf");
        for (TextView textView : textViews) {
            FontManager.markAsIconContainer(textView, FontManager.getTypeface(context, "fontAwesome.ttf"));
        }
    }
}
