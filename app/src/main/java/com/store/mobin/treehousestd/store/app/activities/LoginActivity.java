package com.store.mobin.treehousestd.store.app.activities;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.InputType;
import android.view.View;
import android.view.Window;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.crashlytics.android.Crashlytics;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.store.mobin.treehousestd.store.R;
import com.store.mobin.treehousestd.store.app.models.User;
import com.store.mobin.treehousestd.store.app.utils.utils;

import io.fabric.sdk.android.Fabric;

import static com.store.mobin.treehousestd.store.app.models.User.sharedPreferences;


/**
 * Created by Treehouse.std on 3/6/2017.
 */

public class LoginActivity extends BaseActivity {
    EditText email, password;
    TextView login;
    FirebaseAuth auth;
    FirebaseAuth.AuthStateListener authStateListener;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Fabric.with(this, new Crashlytics());
        initAuth();


    }


    void tryLogin() {

        if (!utils.validInput(email) || !utils.validInput(password)) {

            Toast.makeText(LoginActivity.this, "Input Required", Toast.LENGTH_SHORT).show();

        } else {
            final User user = new User();
            user.setEmail(email.getText().toString());
            user.setPassword(password.getText().toString());

//            Intent loginData = new Intent();
//            loginData.putExtra("username", email.getText().toString().trim());
//            loginData.putExtra("password", password.getText().toString().trim());
            //setResult(RESULT_OK, loginData);
//            final ProgressDialog progressDialog = new ProgressDialog(this);
//            progressDialog.setMessage("Logging in...");
//            progressDialog.setIndeterminate(true);
//            progressDialog.setCancelable(false);
//            progressDialog.show();
            showProgressDialog(this, "Logging in...");

            auth.signInWithEmailAndPassword(user.getEmail(), user.getPassword()).addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                @Override
                public void onComplete(@NonNull Task<AuthResult> task) {
                    if (task.isSuccessful()) {
                        sharedPreferences(LoginActivity.this).edit().putString("email", user.getEmail()).apply(); // save offline
                        sharedPreferences(LoginActivity.this).edit().putString("password", user.getPassword()).apply();
                        closeProgressDialog();
                        Intent openApp = new Intent(LoginActivity.this, ListsActivity.class);
                        openApp.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        openApp.putExtra("txt", "Login");
                        startActivity(openApp);
                        finish();

                    } else {
                        closeProgressDialog();
                        Toast.makeText(LoginActivity.this, task.getException().getMessage(), Toast.LENGTH_LONG).show();
                    }

                }
            });


        }
    }

    void initAuth() {
        auth = FirebaseAuth.getInstance();
        authStateListener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                FirebaseUser firebaseUser = firebaseAuth.getCurrentUser();
                if (firebaseUser == null) {  // signed out
                    promptLogin();

                } else // user signed in
                {
                    Intent app = new Intent(LoginActivity.this, ListsActivity.class);
                    app.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    app.putExtra("txt", "Already logged in");
                    startActivity(app);
                    finish();

                }


            }
        };

    }

    private void promptLogin() {

        setContentView(R.layout.login);
        initializeViews();
        setClickListeners();
        String emailAddress = sharedPreferences(this).getString("email", "");
        String passwordTxt = sharedPreferences(this).getString("password", "");

        email.setText(emailAddress);
        password.setText(passwordTxt);

    }


    @Override
    protected void setClickListeners() {
        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isNetworkConnected())
                    tryLogin();
                else
                    Toast.makeText(v.getContext(), "No Internet Found", Toast.LENGTH_SHORT).show();
            }

        });


    }

    public void forgotPassword(View v) {                                               ///CHANGED

        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.add_task_dialog);
        dialog.show();
        TextView title = (TextView) dialog.findViewById(R.id.dialogTitle);
        title.setText("forgot password".toUpperCase());
        final EditText input = (EditText) dialog.findViewById(R.id.itemName);
        input.setText("");
        input.setHint("Email");
        input.setInputType(InputType.TYPE_TEXT_VARIATION_EMAIL_ADDRESS);
        TextView submit = (TextView) dialog.findViewById(R.id.add);
        submit.setText("submit".toUpperCase());
        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View v) {
                if (utils.validInput(input)) {
                    showProgressDialog(v.getContext(), "Working...");
                    auth.sendPasswordResetEmail(input.getText().toString()).addOnCompleteListener(new OnCompleteListener<Void>() {
                        @Override
                        public void onComplete(@NonNull Task<Void> task) {
                            if (task.isSuccessful()) {
                                Toast.makeText(v.getContext(), "A password reset link has been sent to " + input.getText().toString(), Toast.LENGTH_LONG).show();
                                closeProgressDialog();


                            } else {
                                Toast.makeText(v.getContext(), task.getException().getMessage(), Toast.LENGTH_LONG).show();
                                closeProgressDialog();
                                dialog.dismiss();
                            }
                        }

                    });

                }

            }
        });
        dialog.findViewById(R.id.cancel).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });


    }

    @Override
    protected void onStart() {
        super.onStart();
        auth.addAuthStateListener(authStateListener);
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (auth != null)
            auth.removeAuthStateListener(authStateListener);
    }

    public void openSignUp(View view) {
        startActivity(new Intent(view.getContext(), SignUPActivity.class));
    }

    @Override
    protected void initializeViews() {
        email = (EditText) findViewById(R.id.email);
        password = (EditText) findViewById(R.id.password);
        login = (TextView) findViewById(R.id.login);
//        forgotPassword = (TextView)findViewById(R.id.forgotPassword);
    }
}
