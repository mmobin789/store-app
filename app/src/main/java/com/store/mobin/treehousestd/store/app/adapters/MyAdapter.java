package com.store.mobin.treehousestd.store.app.adapters;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.content.pm.ResolveInfo;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.support.v7.widget.RecyclerView;
import android.text.InputType;
import android.view.Gravity;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.EditText;
import android.widget.PopupMenu;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.share.model.ShareLinkContent;
import com.facebook.share.widget.ShareDialog;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.store.mobin.treehousestd.store.R;
import com.store.mobin.treehousestd.store.app.activities.Friends;
import com.store.mobin.treehousestd.store.app.activities.ListDetailActivity;
import com.store.mobin.treehousestd.store.app.activities.ListsActivity;
import com.store.mobin.treehousestd.store.app.activities.SettingsActivity;
import com.store.mobin.treehousestd.store.app.models.Item;
import com.store.mobin.treehousestd.store.app.models.List;
import com.store.mobin.treehousestd.store.app.models.User;
import com.store.mobin.treehousestd.store.app.utils.utils;

import java.util.ArrayList;
import java.util.LinkedList;

import static com.store.mobin.treehousestd.store.app.models.Item.item2move;
import static com.store.mobin.treehousestd.store.app.models.Item.move;
import static com.store.mobin.treehousestd.store.app.utils.utils.setFontAwesomeOnTextViews;
import static com.store.mobin.treehousestd.store.app.utils.utils.urlEncode;
import static com.store.mobin.treehousestd.store.app.utils.utils.validInput;

/**
 * Created by Treehouse.std on 3/4/2017.
 */

public class MyAdapter extends RecyclerView.Adapter<MyAdapter.MobinViewHolder> {
    private LinkedList<List> listArrayList;
    private LinkedList<Item> itemArrayList;
    private ArrayList<Item> searchItemList = new ArrayList<>();
    private int viewType;
    private Boolean isItems;
    private Activity context;
    private String itemData = "";
    private DatabaseReference databaseReference;
    private ValueEventListener valueEventListener = new ValueEventListener() {
        @Override
        public void onDataChange(DataSnapshot dataSnapshot) {
            Item.clickedListItems.clear();
            for (DataSnapshot dataSnapshots : dataSnapshot.getChildren()) {
                Item item = dataSnapshots.getValue(Item.class);
//                item.setItemName(dataSnapshots.child("Items").child("itemName").getValue()+"");
//                item.setQuantity(dataSnapshots.child("Items").child("quantity").getValue()+"");
                Item.clickedListItems.add(item);
            }

            if (databaseReference != null)
                databaseReference.removeEventListener(valueEventListener);


        }

        @Override
        public void onCancelled(DatabaseError databaseError) {

        }
    };

    public MyAdapter(LinkedList<List> listArrayList, Activity context) {
        this.listArrayList = listArrayList;
        viewType = 0;
        isItems = false;
        this.context = context;

    }

    public MyAdapter(LinkedList<Item> itemArrayList, Boolean isItems, Activity context) {
        this.itemArrayList = itemArrayList;
        this.isItems = isItems;
        this.context = context;
        viewType = 1;
        searchItemList.addAll(itemArrayList);


    }

    private void getItemsOfCurrentList(String id) {
        Item.clickedListItems.clear();
        databaseReference = FirebaseDatabase.getInstance().getReference(context.getString(R.string.app_name)).child("users").child(User.getUserID()).child("Lists").child(id).child("Items");
        databaseReference.addValueEventListener(valueEventListener);
    }

    @Override
    public int getItemViewType(int position) {
        if (viewType == 0)
            return 0;
        else return 1;

    }

    private void shareOnTwitter(MobinViewHolder holder) {
        String message = listArrayList.get(holder.getAdapterPosition()).getName() + "\n" + listArrayList.get(holder.getAdapterPosition()).getDescription() + "\n" + itemData;
        String tweetUrl = String.format("https://twitter.com/intent/tweet?text=%s",
                urlEncode(message));
        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(tweetUrl));

// Narrow down to official Twitter app, if available:
        java.util.List<ResolveInfo> matches = context.getPackageManager().queryIntentActivities(intent, 0);
        for (ResolveInfo info : matches) {
            if (info.activityInfo.packageName.toLowerCase().startsWith("com.twitter")) {
                intent.setPackage(info.activityInfo.packageName);
            }
        }

        context.startActivity(intent);
    }

    private void shareOnWhatsApp(MobinViewHolder holder) {

        String message = listArrayList.get(holder.getAdapterPosition()).getName() + "\n" + listArrayList.get(holder.getAdapterPosition()).getDescription() + "\n" + itemData;
        Intent sendIntent = new Intent();
        sendIntent.setAction(Intent.ACTION_SEND);
        sendIntent.putExtra(Intent.EXTRA_TEXT, message);
        sendIntent.setType("text/plain");
        sendIntent.setPackage("com.whatsapp");
        context.startActivity(sendIntent);
    }

    private void shareOnMessaging(MobinViewHolder holder) {
        String message = listArrayList.get(holder.getAdapterPosition()).getName() + "\n" + listArrayList.get(holder.getAdapterPosition()).getDescription() + "\n" + itemData;
        Intent sendIntent = new Intent(Intent.ACTION_VIEW);
        sendIntent.addCategory(Intent.CATEGORY_DEFAULT);
        sendIntent.setType("vnd.android-dir/mms-sms");
        sendIntent.putExtra("sms_body", message);
        context.startActivity(sendIntent);

    }

    private void shareonFb(MobinViewHolder holder) {
        String msg = "\n" + listArrayList.get(holder.getAdapterPosition()).getDescription()
                + "\n" + itemData;

        ShareLinkContent shareLinkContent = new ShareLinkContent.Builder()
                .setContentUrl(Uri.parse("https://www.yourshoppingapp.com"))
                .setContentTitle(listArrayList.get(holder.getAdapterPosition()).getName())
                .setContentDescription(msg)
                //  .setQuote(context.getString(R.string.app_name))

                .build();

        ShareDialog shareDialog = new ShareDialog(context);

        shareDialog.show(shareLinkContent);


    }

    private void syncList(MobinViewHolder holder) {

        Intent openFriends = new Intent(context, Friends.class);
        openFriends.putExtra("listID", listArrayList.get(holder.getAdapterPosition()).getId());
        context.startActivity(openFriends);


    }

    private void sharePopUp(final MobinViewHolder holder) {

        for (int i = 0; i < Item.clickedListItems.size(); i++) {
            itemData += Item.clickedListItems.get(i).getItemName() + "\t" + Item.clickedListItems.get(i).getQuantity() + "\n";

        }
        Item.clickedListItems.clear();
        PopupMenu popup;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT)
            popup = new PopupMenu(holder.itemView.getContext(), holder.itemView, Gravity.START);
        else popup = new PopupMenu(holder.itemView.getContext(), holder.itemView);
        MenuInflater inflater = popup.getMenuInflater();
        inflater.inflate(R.menu.share, popup.getMenu());
        popup.show();
        popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.shareFB:
                        shareonFb(holder);
                        break;
                    case R.id.shareTwitter:
                        shareOnTwitter(holder);
                        break;
                    case R.id.shareWhatsApp:
                        shareOnWhatsApp(holder);
                        break;
                    case R.id.shareSMS:
                        shareOnMessaging(holder);
                        break;


                }
                return true;
            }
        });

    }

    private void renameDialog(final MobinViewHolder holder) {
        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.add_task_dialog);
        TextView title = (TextView) dialog.findViewById(R.id.dialogTitle);
        title.setText("Rename");
        final EditText itemName = (EditText) dialog.findViewById(R.id.itemName);
        itemName.setText(itemArrayList.get(holder.getAdapterPosition()).getItemName());
        TextView rename = (TextView) dialog.findViewById(R.id.add);
        rename.setText("Rename".toUpperCase());


        rename.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!validInput(itemName)) {
                    Toast.makeText(v.getContext(), itemName.getHint() + " Required", Toast.LENGTH_SHORT).show();
                } else {
                    renameTask(holder, itemName.getText().toString());
                    dialog.dismiss();
                }

                //  Toast.makeText(v.getContext(), getAddedProjectID(), Toast.LENGTH_SHORT).show();


            }
        });
        dialog.findViewById(R.id.cancel).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();

                //     Toast.makeText(v.getContext(),user.getUsername(),Toast.LENGTH_SHORT).show();
            }
        });
        dialog.show();
    }


    private void renameTask(MobinViewHolder holder, String newName) {

        FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference databaseReference = database.getReference(context.getString(R.string.app_name)).child("users").child(User.getUserID()).child("Lists").child(itemArrayList.get(holder.getAdapterPosition()).getListID()).child("Items")
                .child(itemArrayList.get(holder.getAdapterPosition()).getId());
        Item item = new Item();
        item.setItemName(newName);
        item.setId(itemArrayList.get(holder.getAdapterPosition()).getId());
        item.setQuantity(itemArrayList.get(holder.getAdapterPosition()).getQuantity());
        item.setStatus(itemArrayList.get(holder.getAdapterPosition()).getStatus());
        databaseReference.setValue(item);
//        databaseReference.removeValue();
//        DatabaseReference itemRef = database.getReference(context.getString(R.string.app_name)).child("users").child(User.getUserID()).child("Lists").child(itemArrayList.get(holder.getAdapterPosition()).getListID()).child("Items").child(newName);
//        itemRef.setValue(item);
        itemArrayList.clear();
        notifyDataSetChanged();
        //((ListDetailActivity) context).getTasks();
        // ((ListDetailActivity) context).removeEventListener();

    }

    private void settingsPopUp(View v, final MobinViewHolder holder) {
        PopupMenu popup = new PopupMenu(v.getContext(), v);
        MenuInflater inflater = popup.getMenuInflater();
        inflater.inflate(R.menu.options, popup.getMenu());
        popup.show();
        popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.rename:
                        renameDialog(holder);
                        break;
                    case R.id.addBid:
                        loadQuantityDialog(holder);
                        break;
                    case R.id.delete:
                        deleteItemFromFirebase(holder);
                        break;

                    case R.id.copy:
                        copyItem(holder);
                        break;
                    case R.id.move:
                        moveItem(holder);
                        break;
                }

                return true;
            }
        });
    }

    private void moveItem(MobinViewHolder holder) {
        // utils.secondTime = true;
        Intent intent = new Intent(context, ListsActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
        intent.putExtra("itemID", itemArrayList.get(holder.getAdapterPosition()).getId());
        item2move = new Item();
        item2move.setId(itemArrayList.get(holder.getAdapterPosition()).getId());
        item2move.setQuantity(itemArrayList.get(holder.getAdapterPosition()).getQuantity());
        item2move.setItemName(itemArrayList.get(holder.getAdapterPosition()).getItemName());
        item2move.setStatus(itemArrayList.get(holder.getAdapterPosition()).getStatus());
        context.startActivity(intent);
        move = true;
        deleteItemFromFirebase(holder);
        context.finish();


    }

    private void copyItem(MobinViewHolder holder) {
        //   utils.secondTime = true;
        Intent intent = new Intent(context, ListsActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
        intent.putExtra("itemID", itemArrayList.get(holder.getAdapterPosition()).getId());
        item2move = new Item();
        item2move.setId(itemArrayList.get(holder.getAdapterPosition()).getId());
        item2move.setQuantity(itemArrayList.get(holder.getAdapterPosition()).getQuantity());
        item2move.setItemName(itemArrayList.get(holder.getAdapterPosition()).getItemName());
        item2move.setStatus(itemArrayList.get(holder.getAdapterPosition()).getStatus());
        context.startActivity(intent);
        move = true;
        context.finish();

    }


    @Override
    public MobinViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v;
        if (viewType == 0) {
            v = View.inflate(parent.getContext(), R.layout.projects_adapter, null);
        } else {
            v = View.inflate(parent.getContext(), R.layout.items_adapter, null);
        }
        return new MobinViewHolder(v, isItems);
    }

    private void onStatusClick(final MobinViewHolder holder) {
        setFontAwesomeOnTextViews(context, new TextView[]{holder.status});
        holder.status.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                showStatusPopUp(v, holder);
            }
        });

    }

    @Override
    public void onBindViewHolder(final MobinViewHolder holder, int position) {

        if (isItems) { //items/tasks
            getStatuses(holder);
            onStatusClick(holder);
            //  Glide.with(holder.itemView.getContext()).using(new FirebaseImageLoader()).load(User.getUserPic()).into(holder.itemAvatar);
            holder.itemName.setText(itemArrayList.get(position).getItemName());
            holder.itemQuantity.setText(itemArrayList.get(position).getQuantity());

            holder.dots3.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    settingsPopUp(v, holder);

                }
            });

        } else { // Lists
            if (listArrayList.get(position).isFavorite()) {
                holder.favorite.setVisibility(View.VISIBLE);
            } else {
                holder.favorite.setVisibility(View.INVISIBLE);
            }
            holder.projectOptions.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    getItemsOfCurrentList(listArrayList.get(holder.getAdapterPosition()).getId());
                    listDeletePopUp(holder);

                }
            });

            holder.listItem.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (move) {
                        move = false;
                        saveItemToListinFirebase(holder);
                    } else {
                        Intent openDetail = new Intent(v.getContext(), ListDetailActivity.class);
                        utils.listID = listArrayList.get(holder.getAdapterPosition()).getId();
                        openDetail.putExtra("listID", utils.listID);
                        context.startActivity(openDetail);
                    }
                }
            });
            holder.projectName.setText(listArrayList.get(position).getName());
//            if (listArrayList.get(position).getAvatar() == null) {
//                holder.projectPic.setBackgroundColor(Color.parseColor("#505050"));
//            } else {
//                Glide.with(holder.itemView.getContext()).using(new FirebaseImageLoader()).load(listArrayList.get(position).getAvatar()).into(holder.projectPic);
//            }


        }
    }

    private void saveItemToListinFirebase(MobinViewHolder holder) {

        FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference databaseReference = database.getReference(context.getString(R.string.app_name)).child("users").child(User.getUserID()).child("Lists").child(listArrayList.get(holder.getAdapterPosition()).getId()).child("Items")
                .child(item2move.getId());
        databaseReference.setValue(item2move);
    }

    private void getStatuses(MobinViewHolder holder) {
        //String status = "";
        if (itemArrayList.get(holder.getAdapterPosition()).getStatus().toLowerCase().equals("not done")) {
            holder.status.setText(context.getString(R.string.uncheck));
            holder.status.setTextColor(Color.RED);
            //  status = "not done";
        }
        if (itemArrayList.get(holder.getAdapterPosition()).getStatus().toLowerCase().equals("done")) {
            holder.status.setText(context.getString(R.string.checkbox));
            holder.status.setTextColor(Color.parseColor("#45c24c"));
            //status = "done";
        }
        //return status;
    }

    private void saveStatustoFirebase(MobinViewHolder holder, String status) {
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference databaseReference = database.getReference(context.getString(R.string.app_name)).child("users").child(User.getUserID()).child("Lists");
        databaseReference.child(utils.listID).child("Items").child(itemArrayList.get(holder.getAdapterPosition()).getId()).child("status").setValue(status);
        itemArrayList.clear();


    }

    private void deleteItemFromFirebase(MobinViewHolder holder) {
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference databaseReference = database.getReference(context.getString(R.string.app_name)).child("users").child(User.getUserID()).child("Lists").child(utils.listID).child("Items")
                .child(itemArrayList.get(holder.getAdapterPosition()).getId());
        itemArrayList.remove(holder.getAdapterPosition());
        notifyDataSetChanged();
        databaseReference.removeValue();
        itemArrayList.clear();


    }

    private void setQuantityOnFirebase(MobinViewHolder holder, String quantity) {
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference databaseReference = database.getReference(context.getString(R.string.app_name)).child("users").child(User.getUserID()).child("Lists").child(itemArrayList.get(holder.getAdapterPosition()).getListID()).child("Items")
                .child(itemArrayList.get(holder.getAdapterPosition()).getId());
        Item item = new Item();
        item.setQuantity(quantity);
        item.setItemName(itemArrayList.get(holder.getAdapterPosition()).getItemName());
        item.setStatus(itemArrayList.get(holder.getAdapterPosition()).getStatus());
        item.setId(itemArrayList.get(holder.getAdapterPosition()).getId());
        databaseReference.setValue(item);
        itemArrayList.clear();
        notifyDataSetChanged();
    }

    private void loadQuantityDialog(final MobinViewHolder holder) {
        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.add_task_dialog);
        final EditText charges = (EditText) dialog.findViewById(R.id.itemName);
        charges.setInputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_NUMBER_FLAG_DECIMAL);
        charges.setText(itemArrayList.get(holder.getAdapterPosition()).getItemName());
        TextView add = (TextView) dialog.findViewById(R.id.add);
        //      add.setTextSize(context.getResources().getDimension(R.dimen._14ssp));
        add.setText("add ".toUpperCase());
        charges.setHint("Quantity");
        charges.setText("");


        add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!validInput(charges)) {
                    Toast.makeText(v.getContext(), charges.getHint(), Toast.LENGTH_SHORT).show();
                } else {
                    setQuantityOnFirebase(holder, charges.getText().toString());
                    dialog.dismiss();
                }

                //  Toast.makeText(v.getContext(), getAddedProjectID(), Toast.LENGTH_SHORT).show();


            }
        });
        dialog.findViewById(R.id.cancel).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();

                //     Toast.makeText(v.getContext(),user.getUsername(),Toast.LENGTH_SHORT).show();
            }
        });
        dialog.show();

    }

    public void filter(String query) {

        itemArrayList.clear();
        query = query.toLowerCase();
        if (query.length() == 0) {
            itemArrayList.addAll(searchItemList);

        } else {


            for (int i = 0; i < searchItemList.size(); i++) {
                if (searchItemList.get(i).getItemName().toLowerCase().contains(query)) {

                    Item item = new Item();
                    item.setItemName(searchItemList.get(i).getItemName());
                    item.setStatus(searchItemList.get(i).getStatus());
                    item.setListID(searchItemList.get(i).getListID());
                    item.setQuantity(searchItemList.get(i).getQuantity());
                    item.setId(searchItemList.get(i).getId());
                    itemArrayList.add(item);


                }

            }
            notifyDataSetChanged();
        }
    }

    private void showStatusPopUp(View v, final MobinViewHolder holder) {
        PopupMenu popup;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT)
            popup = new PopupMenu(v.getContext(), v, Gravity.END);
        else popup = new PopupMenu(v.getContext(), v);
        MenuInflater inflater = popup.getMenuInflater();
        inflater.inflate(R.menu.status, popup.getMenu());
        popup.show();
        popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.done:
                        if (itemArrayList.get(holder.getAdapterPosition()).getStatus().toLowerCase().equals("not done")) {
                            holder.status.setText(context.getString(R.string.checkbox));
                            holder.status.setTextColor(Color.parseColor("#45c24c"));
                            saveStatustoFirebase(holder, item.getTitle().toString());
                        }
                        break;
                    case R.id.notDone:
                        if (itemArrayList.get(holder.getAdapterPosition()).getStatus().toLowerCase().equals("done")) {
                            holder.status.setText(context.getString(R.string.uncheck));
                            holder.status.setTextColor(Color.RED);
                            saveStatustoFirebase(holder, item.getTitle().toString());
                        }
                        break;
                }

                return true;
            }
        });

    }

    private void deleteProjectFromFirebase(MobinViewHolder holder) {
        DatabaseReference databaseReference = FirebaseDatabase.getInstance().getReference(context.getString(R.string.app_name)).child("users").child(User.getUserID()).child("Lists").child(listArrayList.get(holder.getAdapterPosition()).getId());
        databaseReference.removeValue();
        listArrayList.remove(holder.getAdapterPosition());
        notifyDataSetChanged();
        listArrayList.clear();
    }

    private void renameListDialog(final MobinViewHolder holder) {
        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.add_task_dialog);
        TextView title = (TextView) dialog.findViewById(R.id.dialogTitle);
        title.setText("Rename");
        final EditText itemName = (EditText) dialog.findViewById(R.id.itemName);
        itemName.setText(listArrayList.get(holder.getAdapterPosition()).getName());
        TextView rename = (TextView) dialog.findViewById(R.id.add);
        rename.setText("Rename".toUpperCase());


        rename.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!validInput(itemName)) {
                    Toast.makeText(v.getContext(), itemName.getHint() + " Required", Toast.LENGTH_SHORT).show();
                } else {
                    renameListTask(holder, itemName.getText().toString());
                    dialog.dismiss();
                }
            }
        });
        dialog.findViewById(R.id.cancel).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.show();
    }

    private void renameListTask(MobinViewHolder holder, String newName) {
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference databaseReference = database.getReference(context.getString(R.string.app_name)).child("users").child(User.getUserID()).child("Lists");
        databaseReference.child(listArrayList.get(holder.getAdapterPosition()).getId()).child("name").setValue(newName);

        ((ListsActivity) context).getProjects();
    }

    private void listDeletePopUp(final MobinViewHolder holder) {
        PopupMenu popup;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT)
            popup = new PopupMenu(holder.itemView.getContext(), holder.itemView, Gravity.END);
        else popup = new PopupMenu(holder.itemView.getContext(), holder.itemView);
        MenuInflater inflater = popup.getMenuInflater();
        inflater.inflate(R.menu.delete, popup.getMenu());
        popup.show();
        popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {

                switch (item.getItemId()) {
                    case R.id.deleteOnCLick:
                        deleteProjectFromFirebase(holder);
                        break;
                    case R.id.renameList:
                        renameListDialog(holder);
                        break;
                    case R.id.plan_shopping_date:
                        Intent shoppingIntent = new Intent(context, SettingsActivity.class);
                        shoppingIntent.putExtra("list name", listArrayList.get(holder.getAdapterPosition()).getName());
                        shoppingIntent.putExtra("list id", listArrayList.get(holder.getAdapterPosition()).getId());
                        context.startActivity(shoppingIntent);
                        break;
                    case R.id.favorite_list:
                        changeListFavorite(holder);
                        break;
                    case R.id.share:
                        itemData = "";
                        sharePopUp(holder);
                        break;
                    case R.id.sync:
                        syncList(holder);
                        break;
                }

                return true;
            }
        });
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        if (isItems)
            return itemArrayList.size();
        else return listArrayList.size();

    }

    private void changeListFavorite(MobinViewHolder holder) {
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference databaseReference = database.getReference(context.getString(R.string.app_name)).child("users").child(User.getUserID()).child("Lists");
        boolean checkList = listArrayList.get(holder.getAdapterPosition()).isFavorite();
        checkList = !checkList;
        databaseReference.child(listArrayList.get(holder.getAdapterPosition()).getId()).child("favorite").setValue(checkList);
        listArrayList.clear();
        notifyDataSetChanged();
    }

    class MobinViewHolder extends RecyclerView.ViewHolder {
        TextView projectName, projectOptions, itemName, dots3, status, itemQuantity, favorite;
        RelativeLayout listItem;
        TextView projectPic;
        TextView itemAvatar;

        MobinViewHolder(View itemView, Boolean isItems) {
            super(itemView);
            if (isItems) {

                itemName = (TextView) itemView.findViewById(R.id.subItem);
                itemQuantity = (TextView) itemView.findViewById(R.id.subItemQuantity);
                dots3 = (TextView) itemView.findViewById(R.id.dots3);
                status = (TextView) itemView.findViewById(R.id.checkboxV);
                itemAvatar = (TextView) itemView.findViewById(R.id.profile_image);
                setFontAwesomeOnTextViews(itemView.getContext(), new TextView[]{status, itemAvatar});
            } else {
                listItem = (RelativeLayout) itemView.findViewById(R.id.listlayout);
                projectName = (TextView) itemView.findViewById(R.id.project_name);
                projectOptions = (TextView) itemView.findViewById(R.id.project_arrow);
                projectPic = (TextView) itemView.findViewById(R.id.project_img);
                favorite = (TextView) itemView.findViewById(R.id.favorite_list);
                setFontAwesomeOnTextViews(itemView.getContext(), new TextView[]{projectPic, favorite});
            }


        }
    }
}
