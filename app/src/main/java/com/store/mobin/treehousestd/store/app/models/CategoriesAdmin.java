package com.store.mobin.treehousestd.store.app.models;

import com.bignerdranch.expandablerecyclerview.model.Parent;

import java.util.ArrayList;

/**
 * Created by TreeHousestudio on 5/3/2017.
 */

public class CategoriesAdmin implements Parent<Item> {
    private ArrayList<Item> items;
    private String name;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    private String id;

    public ArrayList<Item> getItems() {
        return items;
    }

    public void setItems(ArrayList<Item> items) {
        this.items = items;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public ArrayList<Item> getChildList() {
        return items;
    }

    @Override
    public boolean isInitiallyExpanded() {
        return false;
    }
}
