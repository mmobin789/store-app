package com.store.mobin.treehousestd.store.app.models;

import java.util.ArrayList;

/**
 * Created by TreeHousestudio on 5/5/2017.
 */

public class Category {
    private String name;
    private ArrayList<Item> items;

    public ArrayList<Item> getItems() {
        return items;
    }

    public void setItems(ArrayList<Item> items) {
        this.items = items;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    private String id;


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
